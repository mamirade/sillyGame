#include <stdlib.h>
#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "dataStructures/list.h"
#include "dataStructures/memoryPool.h"

#include "log.h"
#include "gameLogic/gameObject.h"
#include "physics/physics.h"
#include "graphics/graphics.h"
#include "menu/menu.h"

#define INIT_WINDOW_WIDTH 640
#define INIT_WINDOW_HEIGHT 480

#define MAX_OBJECTS 256
#define TARGET_FPS 50

static int clamp(int val, int min, int max) {
    // On suppose qu'un compilateur peut optimiser ça s'il faut
    if (val < min) {
        return min;
    }
    else if (val > max) {
        return max;
    }
    return val;
}

static gameState gameLoop(SDL_Renderer *rend,
                   memoryPool *gameObjectPool,
                   memoryPool *physObjectPool,
                   list *activeGameObjects,
                   GameObject **danglingSeal,
                   graphicsInfo *graphicsInfo) {

    SDL_Event e = {};
    milliSeconds startTime = 0;
    milliSeconds waitTime = 0;

    // -1: gauche, 0: pas de mouvement, 1: droite
    int horizontalMovement = 0;
    bool releaseSeal = false;    

    int windowWidth = 0;
    int windowHeight = 0;
    SDL_GetRendererOutputSize(rend, &windowWidth, &windowHeight);
    SDL_SetRenderDrawColor(rend, 255,255,255, SDL_ALPHA_OPAQUE);

    double tmp;
    
    if (*danglingSeal == NULL) {
        *danglingSeal = getRandomSeal(gameObjectPool, physObjectPool);
        (*danglingSeal)->center = (Point) {windowWidth/2, windowHeight/4};
        (*danglingSeal)->center =
            asGameSpacePoint(graphicsInfo, &((*danglingSeal)->center));
    }
    
    while (true) {
        startTime = SDL_GetTicks();      
        
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
            case SDL_QUIT:
                return GAME_QUIT;
            case SDL_WINDOWEVENT:
                switch (e.window.event) {
                // Si on a besoin de redessiner la fenêtre
                case SDL_WINDOWEVENT_RESIZED:                
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    SDL_GetRendererOutputSize(rend, &windowWidth, &windowHeight);
                    setWindowSize(graphicsInfo, windowWidth, windowHeight);
                    continue;
                case SDL_WINDOWEVENT_EXPOSED:
                    SDL_RenderPresent(rend);
                    break;
                case SDL_WINDOWEVENT_CLOSE:
                    return GAME_QUIT;
                }
                break;
            case SDL_KEYDOWN:
                if (!e.key.repeat) {
                    switch(e.key.keysym.sym) {
                    case SDLK_LEFT:
                        horizontalMovement--;
                        break;
                    case SDLK_RIGHT:
                        horizontalMovement++;
                        break;
                    case SDLK_RETURN:
                        releaseSeal = true;
                    }
                }
                break;
            case SDL_KEYUP:                
                switch(e.key.keysym.sym) {
                case SDLK_LEFT:
                    horizontalMovement++;
                    break;
                case SDLK_RIGHT:
                    horizontalMovement--;
                    break;
                case SDLK_ESCAPE:
                    return GAME_PAUSED;
                }
                break;
            case SDL_MOUSEMOTION:
                tmp = (*danglingSeal)->center.y;
                (*danglingSeal)->center.x = (double) e.motion.x;
                (*danglingSeal)->center =
                    asGameSpacePoint(graphicsInfo, &(*danglingSeal)->center);
                (*danglingSeal)->center.y = tmp;
                break;
            case SDL_MOUSEBUTTONUP:
                releaseSeal = (e.button.button == SDL_BUTTON_LEFT);
                break;
            }
        }

        if (releaseSeal) {
            double lastSealPosX = (*danglingSeal)->center.x;
            addEndList(activeGameObjects, *danglingSeal);
            
            *danglingSeal = getRandomSeal(gameObjectPool, physObjectPool);
            (*danglingSeal)->center.x = lastSealPosX;
            (*danglingSeal)->center.y =
                -((double) windowHeight)/(4*getPixelsPerMeter(graphicsInfo));

            
            horizontalMovement = 0;
            releaseSeal = false;
        }
        else {
            horizontalMovement = clamp(horizontalMovement, -1, 1);
            // Avance pixel par pixel
            (*danglingSeal)->center.x +=
                4*((double) horizontalMovement)/getPixelsPerMeter(graphicsInfo);
        }
        
        // Mettre les gameObject à jour     
        physicsUpdate(activeGameObjects, gameObjectPool, physObjectPool,
                      1000/TARGET_FPS, _getPhysObject, onSealCollision);
        
      
        SDL_RenderClear(rend);        
        graphicsUpdate(graphicsInfo, rend, activeGameObjects, _getSprite);
        rendererAddSprite(graphicsInfo, rend, (*danglingSeal)->sprite, &((*danglingSeal)->center));
        SDL_RenderPresent(rend);

        // On espère attendre 20 millisecondes
        // On attends au moins une milliseconde, quitte à ce que le jeu ralentisse
        waitTime = SDL_GetTicks() - startTime;
        SDL_Delay((waitTime < 1000/TARGET_FPS)*(1000/TARGET_FPS - waitTime) + (waitTime >= 1000/TARGET_FPS));      
    }
}

// Renvoie vrai si on doit quitter, faux si l'on a demandé à relancer
static bool mainLoop(SDL_Renderer *rend,
                     memoryPool *gameObjectPool,
                     memoryPool *physObjectPool,
                     list *activeGameObjects,
                     menuDependencies *dep,
                     graphicsInfo *info) {
    
    menuOpt opt = startMenu(rend, dep);
    if (opt == QUIT_OPT) return true;
    
    gameState state;
    GameObject *seal = NULL;
    
    while (true) {
        state = gameLoop(
            rend, gameObjectPool, physObjectPool, activeGameObjects, &seal, info);
        
        switch (state) {
        case GAME_STARTED:
            // Ne devrait pas être renvoyé par gameLoop
            LOG_BUG("GAME STARTED INVALID");
            break;
        case GAME_WON:
            opt = victoryMenu(rend, dep);
            break;
        case GAME_LOST:
            opt = gameOverMenu(rend, dep);
            break;
        case GAME_PAUSED:
            opt = pauseMenu(rend, dep);
            break;
        case GAME_QUIT:
            return true;
        }

        switch (opt) {
        case RESTART_OPT:
            return false;
        case QUIT_OPT:
            return true;
        default:
            break;
        }
        
    }
}

static bool initSDL(SDL_Window **w, SDL_Renderer **r, TTF_Font **fonts, char titleFontFile[], char buttonFontFile[]) {

    bool res = true;
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        LOG_ERROR(SDL_GetError());
        goto sdl_error;
    }
    if (IMG_Init(IMG_INIT_JPG) == 0) {
        LOG_ERROR(SDL_GetError());
        goto img_error;
    }
    if (TTF_Init() != 0) {
        LOG_ERROR(TTF_GetError());
        goto ttf_error;
    }

    *w = SDL_CreateWindow("Sealy game",
                          SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED,
                          INIT_WINDOW_WIDTH,
                          INIT_WINDOW_HEIGHT,
                          SDL_WINDOW_RESIZABLE);
    if (*w == NULL) {
        LOG_ERROR(SDL_GetError());
        goto window_error;
    }
    *r = SDL_CreateRenderer(*w, -1, SDL_RENDERER_ACCELERATED);
    if (*r == NULL) {
        LOG_ERROR(SDL_GetError());
        goto renderer_error;
    }
    SDL_SetRenderDrawColor(*r, 255,255,255, SDL_ALPHA_OPAQUE);

    fonts[0] = TTF_OpenFont(titleFontFile, 72);
    if (fonts[0] == NULL) {
        LOG_ERROR(TTF_GetError());
        goto title_error;
    }
    fonts[1] = TTF_OpenFont(buttonFontFile, 72);
    if (fonts[1] == NULL) {
        LOG_ERROR(TTF_GetError());
        goto button_error;
    }
    
    if (0) {
    button_error:
        TTF_CloseFont(fonts[0]);
    title_error:
        SDL_DestroyRenderer(*r);
    renderer_error:
        SDL_DestroyWindow(*w);
    window_error:
        TTF_Quit();
    ttf_error:
        IMG_Quit();        
    img_error:
        SDL_Quit();
    sdl_error:
        res = false;
    }
    return res;
}

static void quitSDL(SDL_Window *window, SDL_Renderer *rend, TTF_Font **fonts) {
    TTF_CloseFont(fonts[0]);
    TTF_CloseFont(fonts[1]);
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(window);
    
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

int main() {
    // Initialisations...
    if (!beginLog()) {
        return EXIT_FAILURE;
    }
    
    SDL_Window *window = NULL;  
    SDL_Renderer *rend = NULL;
    TTF_Font *fonts[2] = {};
    menuDependencies *dep = NULL;
    graphicsInfo *graphicsInfo = NULL;
    
    if (!initSDL(&window, &rend, fonts,
                 "../resources/fonts/good-times.otf",
                 "../resources/fonts/chunk-master/chunk-five.ttf")) {
        LOG_ERROR(SDL_GetError());
        return EXIT_FAILURE;
    }
    if ((dep = initMenuDependencies(rend, fonts[0], fonts[1])) == NULL) {
        quitSDL(window, rend, fonts);
        return EXIT_FAILURE;
    }    
    if ((graphicsInfo = initGraphics(rend, 
                                     INIT_WINDOW_WIDTH, INIT_WINDOW_HEIGHT,
                                     "../resources/sprites")) == NULL) {
        freeMenuDependencies(dep);
        quitSDL(window, rend, fonts);
        return EXIT_FAILURE;
    }  
    
    // Rand fixé (tant que l'on teste)
    srand(0);

    GameObject gameObjArray[MAX_OBJECTS] = {};
    memoryPool gameObjectPool;
    if (!asMemoryPool(&gameObjectPool, gameObjArray, MAX_OBJECTS, sizeof(*gameObjArray))) {
        LOG_ERROR("Memory Pool creation failed\n");
        goto end_of_programm;
    }

    PhysObject physObjArray[MAX_OBJECTS] = {};
    memoryPool physObjectPool;
    if (!asMemoryPool(&physObjectPool, physObjArray, MAX_OBJECTS, sizeof(*physObjArray))) {
        LOG_ERROR("Memory Pool creation failed\n");
        goto end_of_programm;
    }

    GameObject *objects[MAX_OBJECTS] = {};
    list activeGameObjects = asList((void **) objects, MAX_OBJECTS);
    
    do {
        clearMemoryPool(&gameObjectPool);
        clearMemoryPool(&physObjectPool);
        clearList(&activeGameObjects);
        addBordersToList(graphicsInfo,
                         &gameObjectPool, &physObjectPool, &activeGameObjects);
    } while (!mainLoop(rend, &gameObjectPool, &physObjectPool, &activeGameObjects, dep, graphicsInfo));

end_of_programm:
    // Libérations de mémoire
    endGraphics(graphicsInfo);
    freeMenuDependencies(dep);    
    quitSDL(window, rend, fonts);
    
    return 0;
}
