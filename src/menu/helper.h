#ifndef HELPER_H
#define HELPER_H

#include <SDL2/SDL.h>

SDL_Rect centeredDestinationRect(int windowWidth, int topHeight, int textureWidth, int textureHeight);
double buttonScalingFactor(SDL_Texture *texture, double ratioToWindowHeight, int windowWidth, int windowHeight, int *textureWidth, int *textureHeight);

#endif /* HELPER_H */
