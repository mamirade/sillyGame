#include <assert.h>

#include "button.h"
#include "../log.h"
#include "helper.h"

void setButtonPosition(menuButton *button, SDL_Rect *rect) {
    button->x = rect->x;
    button->y = rect->y;
    button->w = rect->w;
    button->h = rect->h;
}

SDL_Texture *createButtonTexture(SDL_Renderer *rend, int maxWidth, double ratioToWindowHeight) {
    
    char buffer[256];
    int w;
    int h;
    SDL_GetRendererOutputSize(rend, &w, &h);

    SDL_Texture *texture = SDL_CreateTexture(rend,
                                      SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET,
                                      (w < maxWidth) ? w : maxWidth+4, ratioToWindowHeight*h);    
    if (texture == NULL) {
        snprintf(buffer, 256, "Couldn't allocate button texture: %s", SDL_GetError());
        LOG_ERROR(buffer);
        return NULL;
    }

    SDL_SetRenderTarget(rend, texture);
    SDL_GetRendererOutputSize(rend, &w, &h);
    
    int x,
        y;
    for (x = 0; x < w; x++) {
        for (y = 0; y < h; ++y) {
            // Pavage de carré 4x4 pixels
            if (x/4 % 2 == 0 && y/4 % 2 == 0) {
                SDL_SetRenderDrawColor(rend, 150, 150, 200, SDL_ALPHA_OPAQUE);
            }
            else {
                SDL_SetRenderDrawColor(rend, 100, 100, 150, SDL_ALPHA_OPAQUE);
            }
            SDL_RenderDrawPoint(rend, x, y);
        }
    }
    SDL_SetRenderDrawColor(rend, 50, 50, 50, SDL_ALPHA_OPAQUE);
    SDL_Rect rect = {0, 0, w, h};
    SDL_RenderDrawRect(rend, &rect);
    SDL_SetRenderTarget(rend, NULL);
    
    return texture;
}

bool isMouseOnButton(const menuButton *button, int mouseX, int mouseY) {    
    return (0 < mouseX - button->x) && (mouseX - button->x < button->w)
        && (0 < mouseY - button->y) && (mouseY - button->y < button->h);
}

bool isMouseEvent(SDL_EventType t) {
    return t == SDL_MOUSEMOTION
        || t == SDL_MOUSEBUTTONDOWN
        || t == SDL_MOUSEBUTTONUP;
}

void updateAfterMouseEvent(menuButton *button, SDL_EventType t) {
    switch (t) {
    case SDL_MOUSEMOTION:
        button->state |= BUTTON_MOUSE_HOVER;
        break;
    case SDL_MOUSEBUTTONDOWN:
        button->state |= BUTTON_MOUSE_PRESSED;
        break;
    case SDL_MOUSEBUTTONUP:
        button->state |= BUTTON_MOUSE_RELEASED;
    default:
        break;
    }
}

/**
 *  Déplace le focus clavier d'un bouton vers le bouton d'indice offset 
 *  après (ou avant si offset < 0) celui-ci.
 *  Si aucun bouton n'a de focus, alors le bouton 0 en aura un.
 */
int focusButtonOffset(menuButton buttons[], int buttonCount, int offset) {
    
    int i;
    for (i = 0; i < buttonCount; ++i) {
        if (buttons[i].state & BUTTON_KEYBOARD_FOCUS) {
            buttons[i].state &= ~BUTTON_KEYBOARD_FOCUS;
            buttons[(i+offset+buttonCount) % buttonCount].state  |= BUTTON_KEYBOARD_FOCUS;
            return i;
        }
    }
    buttons[0].state |= BUTTON_KEYBOARD_FOCUS;
    return 0;
}

/**
 *  Valide parmi les boutons celui qui a le focus
 *  Si aucun n'a le focus, rien n'est fait et on retourne -1
 */
int validateFocusedButton(menuButton buttons[], int buttonCount) {
    
    for (int i = 0; i < buttonCount; ++i) {
        if (buttons[i].state & BUTTON_KEYBOARD_FOCUS) {
            buttons[i].state |= BUTTON_KEYBOARD_VALIDATE;
            return i;
        }
    }
    return -1;
}

/**
 *  Quand on appuye sur échap, on valide l'éventuelle option
 *  CONTINUE_OPT dans les boutons
 */
void escapePressed(menuButton buttons[], int buttonCount) {
    for (int i = 0; i < buttonCount; ++i) {
        if (buttons[i].associated == CONTINUE_OPT) {
            buttons[i].state |= BUTTON_KEYBOARD_VALIDATE;
            return;
        }
    }
}

/**
 *  Si t est un évenement de fenêtre (selon isMouseEvent), data1 et data2 
 *  devront être la position de la souris.
 *
 *  Sinon, t sera supposé être un évenement de clavier et data1 devra être
 *  e.key.keysym.sym
 *
 *  Note: Si la touche appuyée est data1=SDLK_ESCAPE, cette fonction fera
 *  comme si l'éventuel bouton CONTINUE_OPT a été validé
 */
void updateButtonStates(menuButton buttons[], int buttonCount,
                        SDL_EventType t, int data1, int data2) {
    
    if (isMouseEvent(t)) {
        for (int i = 0; i < buttonCount; ++i) {
            if (isMouseOnButton(buttons+i, data1, data2)) {
                updateAfterMouseEvent(buttons+i, t);
            }
            else {
                buttons[i].state &= NO_MOUSE_STATE;
            }
        }
    }
    else if (t == SDL_KEYDOWN) {
        switch (data1) {
        case SDLK_DOWN:
        case SDLK_TAB:
            focusButtonOffset(buttons, buttonCount, 1);
            break;
        case SDLK_UP:
            focusButtonOffset(buttons, buttonCount, -1);
            break;
        case SDLK_RETURN:
            validateFocusedButton(buttons, buttonCount);
            break;        
        }
    }
    else if (t == SDL_KEYUP) {
        switch (data1) {
        case SDLK_ESCAPE:
            escapePressed(buttons, buttonCount);
            break;
        }
    }
    else {
        char buffer[256];
        snprintf(buffer, 256, "Unknown SDL_EventType; %x", t);
        LOG_ERROR(buffer);
    }
}

void setupButton(SDL_Renderer *rend, SDL_Texture *buttonTexture,
                  menuButton *button, SDL_Rect *dest) {

    int windowWidth;
    int textureWidth;
    int textureHeight;

    SDL_GetRendererOutputSize(rend, &windowWidth, NULL);
    SDL_RenderCopy(rend, buttonTexture, NULL, dest);

    // On cherche à ce que le texte du bouton reste "dans" le bouton
    double factor = buttonScalingFactor(button->text,
                                        1, dest->w, dest->h,
                                        &textureWidth, &textureHeight);

    SDL_Rect buttonTextureDest = *dest;
    
    *dest = centeredDestinationRect(windowWidth, dest->y, factor*textureWidth, factor*textureHeight);
    SDL_RenderCopy(rend, button->text, NULL, dest);

    if (button->state & BUTTON_KEYBOARD_FOCUS) {
        SDL_SetRenderDrawColor(rend, 0, 255, 255, SDL_ALPHA_OPAQUE);
        SDL_RenderDrawRect(rend, &buttonTextureDest);
    }
    setButtonPosition(button, dest);
}

menuOpt chosenOpt(menuButton buttons[], int buttonCount) {
    menuOpt opt = NO_OPT;
    
    for (int i = 0; i < buttonCount; ++i) {
        if ((buttons[i].state & (BUTTON_MOUSE_RELEASED | BUTTON_MOUSE_PRESSED))
            == (BUTTON_MOUSE_RELEASED | BUTTON_MOUSE_PRESSED)) {
            opt = buttons[i].associated;
        }
        else if (buttons[i].state & (BUTTON_KEYBOARD_VALIDATE)) {
            opt = buttons[i].associated;
        }
    }
    return opt;
}

void resetStates(menuButton buttons[], int buttonCount) {
    for (int i = 0; i < buttonCount; ++i) {
        buttons[i].state = NO_STATE;
    }
}
