#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <assert.h>
#include <stdarg.h>

#include "menu.h"
#include "button.h"
#include "helper.h"
#include "options.h"
#include "../log.h"
#include "../dataStructures/list.h"

#define TITLE_COUNT 4
#define MAX_BUTTONS_ON_SCREEN 4
#define RATIO_TITLE_WINDOW_HEIGHT 0.2
#define RATIO_BUTTON_WINDOW_HEIGHT 0.15
// Un facteur multiplicatif de la hauteur du titre
#define TITLE_PADDING 1.35

typedef struct menuDependencies {
    const SDL_Color menuTextColor;
    int buttonCount;
    menuButton buttons[MAX_BUTTONS_ON_SCREEN];
    SDL_Texture *buttonText[OPT_COUNT];
    SDL_Texture *titleText[TITLE_COUNT];
    SDL_Texture *buttonTexture;
} menuDependencies;

int maxWidthTextures(SDL_Texture **textures, int n) {
    int i = 0;
    int maxWidth = 0;
    int w;
    for (i = 0; i < n; ++i) {
        SDL_QueryTexture(textures[i], NULL, NULL, &w, NULL);
        if (w > maxWidth) {
            maxWidth = w;
        }
    }
    return maxWidth;
}

void freeTextures(SDL_Texture **textures, int n) {
    int i;
    for (i = 0; i < n; ++i) {
        SDL_DestroyTexture(textures[i]); 
    }
}

bool initTextures(SDL_Renderer* rend,
                  TTF_Font *font,
                  const char *text[], int n,
                  SDL_Texture **textures) {
    
    SDL_Color menuTextColor = {128, 128, 0, 255};
    char buffer[256];
    SDL_Surface *surf;
    int i;
    
    for (i = 0; i < n; ++i) {
        surf = TTF_RenderUTF8_Blended(font, text[i], menuTextColor);
        if (surf != NULL) {
            textures[i] = SDL_CreateTextureFromSurface(rend, surf);
            SDL_FreeSurface(surf);
        }
        if (textures[i] == NULL) {
            snprintf(buffer, 256, "Couldn't allocate button texture: %s",
                     SDL_GetError());
            
            LOG_ERROR(buffer);
            freeTextures(textures, i);
            return false;
        }        
    }
    return true;
}



menuDependencies *initMenuDependencies(SDL_Renderer *rend, TTF_Font *titleFont, TTF_Font *buttonFont) {
    
    menuDependencies *dep = calloc(1, sizeof(*dep));
    if (dep == NULL) {
        LOG_ERROR("Couldn't allocate menuDependencies");
        return NULL;
    }

    const char *titleText[] = {
        "Sealy Game",
        "Pause",
        "Victory !",
        "Defeat...",
    };
    // titleText doit être aussi long qu'il y a de titres selon la constante TITLE_COUNT
    assert(sizeof(titleText) / sizeof(*titleText) == TITLE_COUNT);
    const char *buttonText[] = {
        "Play",
        "Continue",
        "Restart",
        "Options",
        "Quit",       
    };
    // buttonText doit être aussi long qu'il y a de boutons delon la constante OPT_COUNT
    assert(sizeof(buttonText) / sizeof(*buttonText) == OPT_COUNT);

    if (!initTextures(rend, titleFont, titleText, TITLE_COUNT, dep->titleText)) {
        free(dep);
        return NULL;
    }
    if (!initTextures(rend, buttonFont, buttonText, OPT_COUNT, dep->buttonText)) {
        freeTextures(dep->titleText, TITLE_COUNT);
        free(dep);
        return NULL;
    }

    dep->buttonTexture = createButtonTexture(
        rend,
        maxWidthTextures(dep->buttonText, OPT_COUNT),
        RATIO_BUTTON_WINDOW_HEIGHT);
    
    if (dep->buttonTexture == NULL) {
        freeTextures(dep->buttonText, OPT_COUNT);
        freeTextures(dep->titleText, TITLE_COUNT);
        free(dep);
        return NULL;
    }
    
    return dep;
}

void freeMenuDependencies(menuDependencies *dep) {
    freeTextures(dep->buttonText, OPT_COUNT);
    freeTextures(dep->titleText, TITLE_COUNT);
    SDL_DestroyTexture(dep->buttonTexture);
    free(dep);
}

static void updateMenu(SDL_Renderer *rend, menuDependencies *dep, gameState s) {

    int textureWidth;
    int textureHeight;
    int windowWidth;
    int windowHeight;

    double factor;

    SDL_Rect dest;
    
    SDL_GetRendererOutputSize(rend, &windowWidth, &windowHeight);    
    SDL_SetRenderDrawColor(rend, 200, 200, 240, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(rend);

    factor = buttonScalingFactor(dep->titleText[s], RATIO_TITLE_WINDOW_HEIGHT,
                           windowHeight, windowWidth, &textureWidth, &textureHeight);
    dest = centeredDestinationRect(windowWidth, 0,
                                   factor*textureWidth, factor*textureHeight);
    SDL_RenderCopy(rend, dep->titleText[s], NULL, &dest);
    
    for (int buttonIndex = 0; buttonIndex < dep->buttonCount; ++buttonIndex) {
        
        factor = buttonScalingFactor(dep->buttonTexture, RATIO_BUTTON_WINDOW_HEIGHT,
                               windowWidth, windowHeight, &textureWidth, &textureHeight);
        dest = centeredDestinationRect(
            windowWidth,
            (RATIO_BUTTON_WINDOW_HEIGHT*windowHeight + 4)*buttonIndex
            + TITLE_PADDING*RATIO_TITLE_WINDOW_HEIGHT*windowHeight,
            factor*textureWidth, factor*textureHeight);
        setupButton(rend, dep->buttonTexture, dep->buttons+buttonIndex, &dest);
        

        // On initialise les coordonnées du bouton pour qu'elles soient
        // Au même endroit que les boutons affichés

    }
    SDL_RenderPresent(rend);
}

static menuOpt playerSelection(SDL_Renderer *rend, menuDependencies *dep, gameState s) {

    SDL_Event e = {};
    menuOpt opt = NO_OPT;
    int mouseX = 0;
    int mouseY = 0;

    updateMenu(rend, dep, s);
    while (opt == NO_OPT) {        
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
            case SDL_QUIT:
                return QUIT_OPT;
            case SDL_WINDOWEVENT:
                switch (e.window.event) {
                case SDL_WINDOWEVENT_CLOSE:
                    return QUIT_OPT;
                case SDL_WINDOWEVENT_RESIZED:
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                case SDL_WINDOWEVENT_EXPOSED:
                    updateMenu(rend, dep, s);
                    break;
                }
                break;
            case SDL_MOUSEMOTION:
                // Si hover sur un bouton, montrer que c'est cliquable
                mouseX = e.motion.x;
                mouseY = e.motion.y;
                continue;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
                updateButtonStates(dep->buttons, dep->buttonCount,
                                   e.type, mouseX, mouseY);
                updateMenu(rend, dep, s);
                break;
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                if (!e.key.repeat) {
                    updateButtonStates(dep->buttons, dep->buttonCount,
                                       e.type, e.key.keysym.sym, 0);
                    updateMenu(rend, dep, s);
                    break;
                }
            }
        }
        opt = chosenOpt(dep->buttons, dep->buttonCount);
        if (opt == OPTIONS_OPT) {
            optionsMenu(rend);
            resetStates(dep->buttons, dep->buttonCount);
            opt = NO_OPT;
        }
        SDL_Delay(50);
    }
    return opt;
}

void activateButtons(menuDependencies *dep, int buttonCount, ...) {
    dep->buttonCount = buttonCount;
    
    va_list l;
    va_start(l, buttonCount);

    menuOpt opt;

    for (int i = 0; i < buttonCount; ++i) {
        opt = va_arg(l, menuOpt);
        dep->buttons[i].text = dep->buttonText[opt];
        dep->buttons[i].associated = opt;
        dep->buttons[i].state = NO_STATE;
    }
    va_end(l);
}

menuOpt startMenu(SDL_Renderer *rend, menuDependencies *dep) {
    gameState s = GAME_STARTED;
    activateButtons(dep, 3,
                    PLAY_OPT, OPTIONS_OPT, QUIT_OPT);

    menuOpt chosen = playerSelection(rend, dep, s);

    return chosen;
}

menuOpt pauseMenu(SDL_Renderer *rend, menuDependencies *dep) {
    gameState s = GAME_PAUSED;
    activateButtons(dep, 4,
                    CONTINUE_OPT, RESTART_OPT, OPTIONS_OPT, QUIT_OPT);

    menuOpt chosen = playerSelection(rend, dep, s);

    return chosen;
}

menuOpt victoryMenu(SDL_Renderer *rend, menuDependencies *dep) {
    gameState s = GAME_WON;
    activateButtons(dep, 3,
                    RESTART_OPT, OPTIONS_OPT, QUIT_OPT);
    
    menuOpt chosen = playerSelection(rend, dep, s);

    return chosen;
}

menuOpt gameOverMenu(SDL_Renderer *rend, menuDependencies *dep) {
    gameState s = GAME_LOST;
    activateButtons(dep, 3,
                    RESTART_OPT, OPTIONS_OPT, QUIT_OPT);
    
    menuOpt chosen = playerSelection(rend, dep, s);

    return chosen;
}
