#ifndef MENU_H
#define MENU_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "button.h"

typedef enum {
    GAME_STARTED,
    GAME_PAUSED,
    GAME_WON,
    GAME_LOST,
    GAME_QUIT
} gameState;

typedef struct menuDependencies menuDependencies;

menuDependencies *initMenuDependencies(SDL_Renderer *, TTF_Font *titleFont, TTF_Font *buttonFont);

menuOpt startMenu(SDL_Renderer *, menuDependencies *);
menuOpt pauseMenu(SDL_Renderer *, menuDependencies *);
menuOpt victoryMenu(SDL_Renderer *, menuDependencies *);
menuOpt gameOverMenu(SDL_Renderer *, menuDependencies *);

void freeMenuDependencies(menuDependencies *);

#endif /* MENU_H */
