#include <assert.h>
#include "helper.h"

SDL_Rect centeredDestinationRect(int windowWidth, int topHeight, int textureWidth, int textureHeight) {

    int leftPadding = (windowWidth - textureWidth)/2;
    SDL_Rect destination = {
        leftPadding,
        topHeight,
        textureWidth,
        textureHeight
        
    };
    
    return destination;
}

/**
 *  Calcule le facteur multiplicatif par lequel il faut redimensionner une texture 
 *  pour que la texture ait comme hauteur ratioToWindowHeight fois la hauteur de la fenêtre.
 *  Si la texture est plus large que windowWidth, renvoie alors 
 *  le facteur pour que la texture fasse la largeur de la fenêtre -4 pixels.
 *
 *  textureWidth et textureHeight sont des paramètres de sortie.
 *  Ce sont la largeur et la hauteur de la texture (avant redimensionnement).
 */
double buttonScalingFactor(SDL_Texture *texture, double ratioToWindowHeight, int windowWidth, int windowHeight, int *textureWidth, int *textureHeight) {

    assert(textureWidth != NULL && textureHeight != NULL);
    double scalingFactor;

    SDL_QueryTexture(texture, NULL, NULL, textureWidth, textureHeight);
    scalingFactor = ratioToWindowHeight*((double) windowHeight / *textureHeight);
    if (*textureWidth*scalingFactor > windowWidth) {
        scalingFactor = ((double) windowWidth - 4) / *textureWidth;
    }

    return scalingFactor;
}
