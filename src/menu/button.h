#ifndef BUTTON_H
#define BUTTON_H

#include <SDL2/SDL.h>
#include <stdbool.h>

#include "../dataStructures/event.h"

typedef enum {
    NO_OPT = -1,
    PLAY_OPT = 0,
    CONTINUE_OPT,
    RESTART_OPT,
    OPTIONS_OPT,
    QUIT_OPT,
    OPT_COUNT, // Nombre total d'options (sauf NO_OPT)
} menuOpt;

typedef enum {
    NO_STATE = 0,
    
    NO_MOUSE_STATE = 0b11110000,
    BUTTON_MOUSE_HOVER = 0b00000001,
    BUTTON_MOUSE_PRESSED = 0b00000010,
    BUTTON_MOUSE_RELEASED= 0b00000100,

    NO_KEYBOARD_STATE = 0b00001111,
    BUTTON_KEYBOARD_FOCUS = 0b00010000,
    BUTTON_KEYBOARD_VALIDATE = 0b00100000,
} buttonState;

typedef struct {
    SDL_Texture *text;
    menuOpt associated;
    buttonState state;
    int x;
    int y;
    int w;
    int h;
} menuButton;

SDL_Texture *createButtonTexture(SDL_Renderer *rend, int maxWidth, double ratioToWindowHeight);
void setupButton(SDL_Renderer *rend, SDL_Texture *buttonTexture, menuButton *button, SDL_Rect *destination);

bool isMouseOnButton(const menuButton *button, int mouseX, int mouseY);
void updateButtonStates(menuButton buttons[], int buttonCount, SDL_EventType t, int mouseX, int mouseY);
menuOpt chosenOpt(menuButton buttons[], int buttonCount);
void resetStates(menuButton buttons[], int buttonCount);

#endif /* BUTTON_H */
