#ifndef PHYSICSCALC_H
#define PHYSICSCALC_H

#include <stdint.h>

#include "shape.h"
#include "collision.h"
#include "../dataStructures/event.h"

// uint32_t pour correspondre au millisecondes de SDL2
typedef uint32_t milliSeconds;

typedef enum {
    NO_FORCES = 0b0001,
} physOptions;

typedef enum {
    GROUNDED = 0b0001,
    MARKED_FOR_DELETE = 0b0010,
} physState;

typedef struct {
    physOptions options:4;
    physState state:4;
} physInfo;

typedef struct PhysObject {
    double mass;
    double bounciness;
    Vector speed;
    
    Collider collider;
    physInfo info;
    void (*resolveOverlap)(struct PhysObject *, struct PhysObject *, Vector n1, Vector n2, milliSeconds deltaTime);
} PhysObject;

void applyForces(PhysObject *o, const milliSeconds deltaTime);
void applyForceToPhysObject(PhysObject *p, const Vector force, const milliSeconds deltaTime);
void handleCollision(PhysObject *col1, Vector normal2);

#endif /* PHYSICSCALC_H */
