#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "collision.h"
#include "../log.h"

/**
 *  Renvoie -1 ou 1 selon le signe de x (On dit que 0 est positif, on veut pas de signe "nul")
 */
#define SIGN(x) (((x) >= 0) - ((x) < 0))
/**
 *  Renvoie vrai si a et b sont égaux à une constante près
 */
#define DOUBLE_EQ(a, b) (fabs((a) - (b)) < 1e-5)

/**
 *  Initialise un collider sur le tas avec un centre et une forme donnée. Renvoie NULL si le collider est inconnu
 */

Collider *initCollider(Collider *collider, Shape shape) {
    
    collider->shape = shape;
    
    switch (shape.type) {
    case CIRCLE:
        collider->isColliding = isCircleColliding;
        break;
    case RECT:
        collider->isColliding = isRectangleColliding;
        break;
    case HALF_PLANE:
        collider->isColliding = isHalfPlaneColliding;
        break;
    default:
        // Forme inconnue, on refuse la création de la forme
        LOG_BUG("UNKNOWN COLLIDER SHAPE");
        exit(1);
    }

    return collider;
}

/**
 *  Suppose que circleCollider est un cercle.
 *  @returns true si les deux éléments sont en collision.
 *  @warning Si cette fonction ne connaît pas la nature du collider en face,
 *   elle lui demande alors sa gestion de la collision avec un cercle. Il
 *   est obligatoire pour ne pas entrer en boucle infinie de gérer le cercle quand vous créez un nouveau collider.
 */
bool isCircleColliding(const Collider *circleCollider, const Collider *otherCollider, Point *collisionPoint) {
    
    switch (otherCollider->shape.type) {
    case CIRCLE:
        return circleCircleCollision(
            &circleCollider->shape,
            &otherCollider->shape, collisionPoint);
    case RECT:
        return circleRectangleCollision(
            &circleCollider->shape,
            &otherCollider->shape, collisionPoint);
    case HALF_PLANE:
        return circleHalfPlaneCollision(
            &circleCollider->shape,
            &otherCollider->shape, collisionPoint);
    default:
        LOG_BUG("UNKNOWN COLLIDER COLLISION");
        exit(1);
    }
}

bool isRectangleColliding(const Collider *rectCollider, const Collider *otherCollider, Point *collisionPoint) {
    

    switch (otherCollider->shape.type) {
    case CIRCLE:
        return circleRectangleCollision(
            &otherCollider->shape,
            &rectCollider->shape, collisionPoint);
    case RECT:
        return rectangleRectangleCollision(
            &rectCollider->shape,
            &otherCollider->shape, collisionPoint);
        break;
    case HALF_PLANE:
        return rectangleHalfPlaneCollision(
            &rectCollider->shape,
            &otherCollider->shape, collisionPoint);
    default:
        LOG_BUG("UNKNOWN COLLIDER COLLISION");
        exit(1);
    }
}

bool isHalfPlaneColliding(const Collider *halfPlaneCollider, const Collider *otherCollider, Point *collisionPoint) {
    switch (otherCollider->shape.type) {
    case CIRCLE:        
        return circleHalfPlaneCollision(
            &otherCollider->shape,
            &halfPlaneCollider->shape, collisionPoint);
    case RECT:
        return rectangleHalfPlaneCollision(
            &otherCollider->shape,
            &halfPlaneCollider->shape, collisionPoint);
    case HALF_PLANE:
        return halfPlaneHalfPlaneCollision(
            &halfPlaneCollider->shape,
            &otherCollider->shape, collisionPoint);
    default:        
        LOG_BUG("UNKNOWN COLLIDER COLLISION");
        exit(1);
    }
}

static bool rectangleInscribedInCircle(const Shape *circ, const Shape *rect) {

    const Vector dim = rect->rectangle.distToCenter;
    const double cx = circ->center->x;
    const double cy = circ->center->y;
    const double rx = rect->center->x;
    const double ry = rect->center->y;

    const double squareX1 = (rx-dim.x-cx)*(rx-dim.x-cx);
    const double squareX2 = (rx+dim.x-cx)*(rx+dim.x-cx);
    const double squareY1 = (ry-dim.y-cy)*(ry-dim.y-cy);
    const double squareY2 = (ry+dim.y-cy)*(ry+dim.y-cy);

    const double r2 = circ->circle.radius*circ->circle.radius;
    
    return (squareX1+squareY1 < r2)  // Coin bas gauche dans le cercle ?
        && (squareX1+squareY2 < r2)  // Coin haut gauche dans le cercle ?
        && (squareX2+squareY1 < r2)  // Coin bas droit dans le cercle ?
        && (squareX2+squareY2 < r2); // Coin haut droit dans le cercle ?
}

static void findCircleRectangleColPoint(const Shape *circ, const Shape *rect, Point *collisionPoint) {

    // Cette fonction est horriblement longue et répétitive...
    // Je ne vois pas comment l'améliorer à part peut-être en précalculant les valeurs
    // Mais on perdrait l'intêret de sortir en avance si on a trouvé 2 points corrects

    if (collisionPoint) {
        collisionPoint->x = 0;
        collisionPoint->y = 0;
    }
    int pointsFound = 0;
    
    const Vector dim = rect->rectangle.distToCenter;    
    const double cx = circ->center->x;
    const double cy = circ->center->y;
    const double rx = rect->center->x;
    const double ry = rect->center->y;
    const double r2 = circ->circle.radius*circ->circle.radius;

    double tmp;
    double y;
    // Bord gauche
    double x = rx - dim.x;
    tmp = sqrt(r2 - (x - cx)*(x - cx));
    y = tmp + cy;
    if (fabs(y - ry) < dim.y) {
        collisionPoint->x = x;
        collisionPoint->y = y;
        pointsFound++;
    }
    y = -tmp + cy;
    if (fabs(y - ry) < dim.y) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }

    // Bord droit
    x = rx + dim.x;
    tmp = sqrt(r2 - (x - cx)*(x - cx));
    y = tmp + cy;
    if (fabs(y - ry) < dim.y) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }
    y = -tmp + cy;
    if (fabs(y -ry) < dim.y) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }

    // Bord bas
    y = ry - dim.y;
    tmp = sqrt(r2 - (y - cy)*(y - cy));
    x = tmp + cx;
    if (fabs(x - rx) < dim.x) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }
    x = -tmp + cx;
    if (fabs(x - rx) < dim.x) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }

    // Bord haut
    y = ry + dim.y;
    tmp = sqrt(r2 - (y - cy)*(y - cy));
    x = tmp + cx;
    if (fabs(x - rx) < dim.x) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }
    x = -tmp + cx;
    if (fabs(x - rx) < dim.x) {
        collisionPoint->x += x;
        collisionPoint->y += y;
        pointsFound++;
        if (pointsFound >= 2) {collisionPoint->x /= 2; collisionPoint->y /= 2; return;}
    }
}

bool circleRectangleCollision(const Shape *circ, const Shape *rect, Point *collisionPoint) {

    const Vector dim = rect->rectangle.distToCenter;
    
    // (x, y) sont les coordonnées du vecteur allant des centres du rectangle au cercle    
    const double x = circ->center->x - rect->center->x;
    const double y = circ->center->y - rect->center->y;

    const double r = circ->circle.radius;

    const double absX = fabs(x);
    const double absY = fabs(y);
    const bool collide = (
        // Si la collision éventuelle a lieu sur les bords gauche/demi-plan du rectangle
        ((absX < dim.x) && (absY < r + dim.y)) ||
        // Si elle a eu lieu sur les bords supérieurs/inférieurs
        ((absY < dim.y) && (absX < r + dim.x)) ||
        // Si elle a eu lieu au niveau de ses coins
        (hypot(absX - dim.x, absY - dim.y) < r));
    
    if (collide && collisionPoint) {
        
        // Si une figure est entièrement comprise dans l'autre,
        // on ne peut pas définir de bon point de collision
        // Par simplicité, on prend juste le centre d'une des deux figures
        if ((fabs(rect->center->x - circ->center->x) < fabs(dim.x-r)
             && fabs(rect->center->y - circ->center->y) < fabs(dim.y-r))
            || rectangleInscribedInCircle(circ, rect)) {
            
            collisionPoint->x = circ->center->x;
            collisionPoint->y = circ->center->y;
        }
        else {
            findCircleRectangleColPoint(circ, rect, collisionPoint);            
        }
    }
    
    return collide;
}

bool circleCircleCollision(const Shape *circ1, const Shape *circ2, Point *collisionPoint) {

    // (x, y) sont les coordonnées du vecteur allant de c2 à c1, les centres des cercles
    const double x = circ1->center->x - circ2->center->x;
    const double y = circ1->center->y - circ2->center->y;

    // Distance au carré, on s'épargne une racine carrée
    double distance = x*x+y*y;

    const bool collide =
        distance < (circ1->circle.radius + circ2->circle.radius) * (circ1->circle.radius + circ2->circle.radius);

    if (collide && collisionPoint) {

        // Distance réelle, elle est nécessaire au calcul
        // si on suppose que les formes ne peuvent pas se chevaucher, on pourrait supposer que distance ~= r1 + r2
        distance = sqrt(distance);
        
        /* longMilieu: Longueur de l'intersection des deux cercles sur le segment entre leurs deux centres
           Distance = r1 + r2 - longMilieu
           longMilieu = r1 + r2 - distance 
           Donc, en partant du centre de r2, on a que le point de collision est à une distance 
           r2 - longMilieu/2 = (r2-r1+d)/2*/
        const double colDist = (circ2->circle.radius - circ1->circle.radius + distance)/2;
        /* On norme le vecteur (x, y), on lui donne comme longueur
           la distance depuis le centre d'un des cercles vers le point central de l'intersection,
           et on ajuste l'origine pour que celui-ci représente le point de collision */
        collisionPoint->x = fma(x/distance, colDist, circ2->center->x);
        collisionPoint->y = fma(y/distance, colDist, circ2->center->y);
    }
    return collide;
}

bool rectangleRectangleCollision(const Shape *rect1, const Shape *rect2, Point *collisionPoint) {

    // (x, y) sont les coordonnées du vecteur allant de c2 à c1, les centres des rectangles
    const double x = rect1->center->x - rect2->center->x;
    const double y = rect1->center->y - rect2->center->y;

    // On s'intéresse au distance depuis le centre plutôt que les longueurs du rectangle
    const Vector dim1 = rect1->rectangle.distToCenter;
    const Vector dim2 = rect2->rectangle.distToCenter;

    /* Il y a collision si la distance entre les deux centres est plus petite que 
       la somme des distances entre le centre et les bords du rectange */
    const bool collide =
        (fabs(x) < dim1.x + dim2.x) && (fabs(y) < dim1.y + dim2.y);

    if (collide && collisionPoint) {
        /* Le problème est le même horizontalement et verticalement
           Soient
           s = SIGN(x ou y),
           d(1|2) = dim(1|2).x ou dim(1|2).y
           r2 = rect2->center.x ou rect2->center.y
           Soit c la distance séparant les deux bords s'intersectant
           
           On a |c| = d2 + d1 + |x|
           On propose de mettre c de même sens que x, donc c = s*(d1 + d2) + x
           Donc, le point de collision se situe à r2 + s*d2 - c, ce qui se simplifie en: 
        */
        collisionPoint->x =
            fma(0.5, fma(SIGN(x), dim2.x - dim1.x, x), rect2->center->x);
        collisionPoint->y =
            fma(0.5, fma(SIGN(y), dim2.y - dim1.y, y), rect2->center->y);
    }

    return collide;
}

bool circleHalfPlaneCollision(const Shape *circ, const Shape *halfPlane, Point *collisionPoint) {

    const double x0 = halfPlane->center->x;
    const double y0 = halfPlane->center->y;
    const double dx = halfPlane->halfPlane.dirVector.x;
    const double dy = halfPlane->halfPlane.dirVector.y;

    const double xc = circ->center->x;
    const double yc = circ->center->y;

    /*
     HalfPlane et circ sont en collision si le point le plus proche du centre C
     appartenant à la demi-plan est un point du cercle
     i.e. on cherche le point correspondant à la projection orthogonale de C sur halfPlane

     Ce point P vérifie xp = x0 + t*dx
                        yp = y0 + t*dy
     Et, puisque P est le projeté orthogonal de C sur halfPlane, on a que le produit 
     scalaire entre le vecteur CP et dirVector est nul.
     i.e:  dx(xp - xc) + dy(yp - yc) = 0 <=> dx(x0 + t*dx - xc) + dy(y0 + t*dy - yc) = 0

     Ce qui donne comme solution pour t:
    */

    const double t = (dx*(xc - x0) + dy*(yc - y0))/(dx*dx + dy*dy);
    // Donc, P vaut:
    const double xp = x0 + t*dx;
    const double yp = y0 + t*dy;

    // Si P n'est pas dans le cercle
    if ((xp - xc)*(xp - xc) + (yp - yc)*(yp - yc)
        > circ->circle.radius*circ->circle.radius
        // Ou si le centre est à droite du demi-plan
        // (c-à-d, produit scalaire entre la normale à gauche du vecteur directeur du plan
        // et le vecteur entre le centre du cercle et le centre de la droite <= 0)
        && (dy*(xc-x0) - dx*(yc-y0) > 0)) {
        return false;
    }
    // P est également le point de collision
    else if (collisionPoint) {
        collisionPoint->x = xp;
        collisionPoint->y = yp;
    }
    return true;
}

static bool pointInRectangle(const Point *p, const Shape *rect) {
    return fabs(p->x - rect->center->x) < rect->rectangle.distToCenter.x
        && fabs(p->y - rect->center->y) < rect->rectangle.distToCenter.y;
}

bool rectangleHalfPlaneCollision(const Shape *rect, const Shape *halfPlane, Point *collisionPoint) {

    const double rectXmin = rect->center->x - rect->rectangle.distToCenter.x;
    const double rectXmax = rect->center->x + rect->rectangle.distToCenter.x;
    const double rectYmin = rect->center->y - rect->rectangle.distToCenter.y;
    const double rectYmax = rect->center->y - rect->rectangle.distToCenter.y;

    if (collisionPoint) {
        collisionPoint->x = 0;
        collisionPoint->y = 0;
    }
    
    // Coin haut gauche du rectangle
    Point center = {rectXmax, rectYmin};
    
    Shape halfPlaneTmp = initHalfPlaneShape((Vector) {0, 0}, &center);
    Point col;

    bool collide = false;

    // On regarde pour chaque côté du rectangle s'il y a intersection

    // Côté haut (vu comme une demi-plan de vecteur directeur (1, 0))
    halfPlaneTmp.halfPlane.dirVector.x = 1;
    if (halfPlaneHalfPlaneCollision(&halfPlaneTmp, halfPlane, &col)
        && pointInRectangle(&col, rect)) {
        collide = true;
        // Si on cherche pas à récupérer le point de collision, on peut quitter
        if (!collisionPoint) return true;
        collisionPoint->x = col.x;
        collisionPoint->y = col.y;
    }

    // Côté gauche (vu comme une demi-plan d = k*(0, 1))
    halfPlaneTmp.halfPlane.dirVector.x = 0;
    halfPlaneTmp.halfPlane.dirVector.y = 1;
    if (halfPlaneHalfPlaneCollision(&halfPlaneTmp, halfPlane, &col)
        && pointInRectangle(&col, rect)) {
        collide = true;
        // Si on cherche pas à récupérer le point de collision, on peut quitter
        if (!collisionPoint) return true;
        collisionPoint->x += col.x;
        collisionPoint->y += col.y;
    }

    // On se place du point de vue du côté bas droit
    center.x = rectXmin;
    center.y = rectYmax;
    
    // Côté droit (vu comme une demi-plan d = k*(0, -1))
    halfPlaneTmp.halfPlane.dirVector.y = -1;
    if (halfPlaneHalfPlaneCollision(&halfPlaneTmp, halfPlane, &col)
        && pointInRectangle(&col, rect)) {
        collide = true;
        // Si on cherche pas à récupérer le point de collision, on peut quitter
        if (!collisionPoint) return true;
        collisionPoint->x += col.x;
        collisionPoint->y += col.y;
    }
    
    halfPlaneTmp.halfPlane.dirVector.x = -1;
    halfPlaneTmp.halfPlane.dirVector.y = 0;
    // Côté bas (vu comme une demi-plan d = k*(-1, 0))
    if (halfPlaneHalfPlaneCollision(&halfPlaneTmp, halfPlane, &col)
        && pointInRectangle(&col, rect)) {
        collide = true;
        // Si on cherche pas à récupérer le point de collision, on peut quitter
        if (!collisionPoint) return true;
        collisionPoint->x += col.x;
        collisionPoint->y += col.y;
    }

    // On a fait la somme de tous le points touchant les bords du rectangle
    // Il y en a toujours 2 s'il y a eu collision (ou 0 s'il y a pas de collision,
    // mais collisionPoint peut alors valoir n'importe quoi)
    if (collisionPoint) {
        collisionPoint->x /= 2;
        collisionPoint->y /= 2;
    }

    return collide;
}

// Le point de collision est ici le point d'intersection entre les deux droites directrices
bool halfPlaneHalfPlaneCollision(const Shape *halfPlane1, const Shape *halfPlane2, Point *collisionPoint) {

    const double x1 = halfPlane1->center->x;
    const double y1 = halfPlane1->center->y;
    const double x2 = halfPlane2->center->x;
    const double y2 = halfPlane2->center->y;

    const double dx1 = halfPlane1->halfPlane.dirVector.x;
    const double dy1 = halfPlane1->halfPlane.dirVector.y;
    const double dx2 = halfPlane2->halfPlane.dirVector.x;
    const double dy2 = halfPlane2->halfPlane.dirVector.y;

    const double det = (dx2*dy1 - dx1*dy2);

    // Demi-Plans parallèles (relativement à leurs droites directrices)
    if (DOUBLE_EQ(det, 0)) {
        // Demi-Plans confondus (relativement à leurs droites directrices)
        if (DOUBLE_EQ((x1 - x2)/dx1, (y1 - y2)/dy1)
            // Demi-Plans parallèles de même sens
            || DOUBLE_EQ(dx1, dx2)) {
            if (collisionPoint) {
                collisionPoint->x = (x1 + x2)/2;
                collisionPoint->y = (y1 + y2)/2;
            }
            return true;
        }
        // Demi-plans parallèles de sens opposés
        else {
            return false;
        }
    }
    if (collisionPoint) {
        /*
         *  On cherche le point de collision C:(xc, yc) qui appartient au deux demi-plans
         * 
         *  On a donc:
         *    
         *    xc = x1 + t1*dx1 = x2 + t2*dx2 <=> dx1*t1 - dx2*t2 = x2 - x1
         *    yc = y1 + t1*dy1 = y2 + t2*dy2 <=> dy1*t1 - dy2*t2 = y2 - y1
         *
         *  Ce système a pour solution en t1 (t2 inutile):
         */
        const double t1 = (1/det)*(-dy2*(x2-x1)+dx2*(y2-y1));

        // Et donc le point de collision vaut:
        collisionPoint->x = x1 + t1*dx1;
        collisionPoint->y = y1 + t1*dy1;
    }
    return true;
}
