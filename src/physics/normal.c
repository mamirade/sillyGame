#include <math.h>
#include <stdlib.h>

#include "normal.h"
#include "collision.h"
#include "../log.h"

/**
 *  Renvoie le vecteur normal au collider à un point donné. 
 *  Cette fonction sert seulement à appeler la fonction correspondant à la forme de collider
 */
Vector getNormal(const Collider *col, const Point *collisionPoint) {

    Vector v;
    
    switch (col->shape.type) {
    case CIRCLE:
        v = circleNormal(&col->shape, collisionPoint);
        break;
    case RECT:
        v = rectNormal(&col->shape, collisionPoint);
        break;
    case HALF_PLANE:
        v = halfPlaneNormal(&col->shape, collisionPoint);
        break;
    default:
        // Forme inconnue, on ne peut pas trouver une normale
        LOG_BUG("UNKNOWN COLLIDER SHAPE");
        exit(1);
    }
    return v;
}

/**
 *  Récupère la normale à la Shape passée en paramètre (qui sera supposée un rectangle) au point de collision donné.
 *  getNormal devrait être préférée (à moins de connaître la forme du Collider, ou juste récupérer une normale de Shape sans Collider attaché)
 */
Vector rectNormal(const Shape *rect, const Point *collisionPoint) {

    const Vector dim = rect->rectangle.distToCenter;
    
    const Vector offsetToRectCenter = {
        collisionPoint->x - rect->center->x,
        collisionPoint->y - rect->center->y
    };

    // Fonctions affines des diagonales du rectangles
    const double y1 = (dim.y/dim.x)*offsetToRectCenter.x;
    const double y2 = -(dim.y/dim.x)*offsetToRectCenter.x;
    
    const Vector v = {
        // Entre les deux diagonales du rectangle
        (offsetToRectCenter.y <= y1 && offsetToRectCenter.y >= y2) -
        (offsetToRectCenter.y >= y1 && offsetToRectCenter.y <= y2),
        // Sur ou sous les deux diagonales du rectangle
        (offsetToRectCenter.y >= y1 && offsetToRectCenter.y >= y2) -
        (offsetToRectCenter.y <= y1 && offsetToRectCenter.y <= y2)
    };
    
    return v;
}

/**
 *  Récupère la normale à la Shape passée en paramètre (qui sera supposée un cercle) au point de collision donné.
 *  getNormal devrait être préférée (à moins de connaître la forme du Collider, ou juste récupérer une normale de Shape sans Collider attaché)
 */
Vector circleNormal(const Shape *circ, const Point *collisionPoint) {

    Vector n = {
        collisionPoint->x - circ->center->x,
        collisionPoint->y - circ->center->y
    };

    // Dans le cas général, si l'on supposait que les formes ne peuvent pas se chevaucher,
    // alors ce serait juste le rayon du cercle
    // On pourrait éventuellement faire cette approximation
    const double norm = hypot(n.x, n.y);

    n.x /= norm;
    n.y /= norm;

    return n;
}

/**
 *  Récupère la normale à la Shape passée en paramètre (qui sera supposée une demi-plan) au point de collision donné.
 *  getNormal devrait être préférée (à moins de connaître la forme du Collider, ou juste récupérer une normale de Shape sans Collider attaché)
 *
 *  NOTE: La normale d'une demi-plan étant ambiguë, cette fonction renvoie toujours la normale 
 *  "à demi-plan" de la demi-plan elle-même.
 */
Vector halfPlaneNormal(const Shape *halfPlane, const Point *collisionPoint) {

    // Peut importe le point de collision, la normale est la même
    (void) collisionPoint;

    // halfPlane a déjà un vecteur directeur normé, donc sa normale "à demi-plan" est simplement:
    Vector n = {
        halfPlane->halfPlane.dirVector.y,
        -halfPlane->halfPlane.dirVector.x
    };
    
    return n;
}
