#ifndef COLLISION_H
#define COLLISION_H

#include <stdbool.h>
#include "shape.h"

typedef struct Collider {
    Shape shape;
    bool (*isColliding)(const struct Collider *, const struct Collider *, Point *);
} Collider;

Collider *initCollider(Collider*, Shape);

bool circleCircleCollision(const Shape *, const Shape *, Point *);
bool circleRectangleCollision(const Shape *, const Shape *, Point *);
bool circleHalfPlaneCollision(const Shape *, const Shape *, Point *);

bool rectangleRectangleCollision(const Shape *, const Shape *, Point *);
bool rectangleHalfPlaneCollision(const Shape *, const Shape *, Point *);

bool halfPlaneHalfPlaneCollision(const Shape *, const Shape *, Point *);

bool isCircleColliding(const Collider *, const Collider *, Point *);
bool isRectangleColliding(const Collider *, const Collider *, Point *);
bool isHalfPlaneColliding(const Collider *, const Collider *, Point *);

#endif /* COLLISION_H */
