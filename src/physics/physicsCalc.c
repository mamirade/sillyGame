#include <stdio.h>
#include <math.h>

#include "physicsCalc.h"

#define MIN_SPEED 1e-4
#define SPEED_THRESHOLD 1
#define MAX_FRAMES_ASLEEP 6

const Vector gravity = {0, -9.81};
const double airResistanceCoefficient = 0.5;

/**
 *  Applique toutes les forces qui doivent constamment être appliqués
 *  sur un objet (ex: gravité, résistance de l'air...).
 */
void applyForces(PhysObject *physObj, const unsigned deltaTime) {

    if (physObj->info.options & NO_FORCES) return;
    
    Vector objectGravity = gravity;
    objectGravity.y *= physObj->mass;
    
    // La résistance de l'air dépend de la vitesse de l'objet étudié
    Vector airResistance = {
        -airResistanceCoefficient * physObj->speed.x,
        -airResistanceCoefficient * physObj->speed.y,
    };

    // La résistance de l'air est quadratique en fonction de la
    // vitesse de l'objet s'il est assez rapide
    if (physObj->speed.x > SPEED_THRESHOLD) {
        airResistance.x *= fabs(physObj->speed.x);
    }
    if (physObj->speed.y > SPEED_THRESHOLD) {
        airResistance.y *= fabs(physObj->speed.y);
    }
    
    // Liste de force appliqués à chaque passage de boucle
    if (~physObj->info.state & GROUNDED) {
        applyForceToPhysObject(physObj, objectGravity, deltaTime);
    }
    applyForceToPhysObject(physObj, airResistance, deltaTime);    
}

/**
 *  Applique la force passée en paramètre à l'objet
 */
void applyForceToPhysObject(PhysObject *physObj, const Vector force, const unsigned deltaTime) {
    
    const double mass = physObj->mass;
    /*
      On a que l'accélération est liée à la force par Somme(Vect(F)) = m*Vect(a).

      Supposons que l'on ait une seule force, que l'on notera F pour alléger la notation

      L'accélération est définie comme la dérivée de la vitesse. 
      Donc a = (v_f - v_i) / deltaTime avec v_f la vitesse finale et v_i la vitesse initiale.
  
      Donc v_f = a*deltaTime + v_i = (F/m)*deltaTime + v_i
  
      Si une autre force venaît à s'ajouter à F à un instant donné, alors la
      vitesse finale serait correcte car le v_f de la précédente force devient 
      le v_i de la force courante, donc la formule ci-dessus fonctionne si on
      applique plusieurs forces sur l'objet

      C'est cette formule qui sera employé dans notre simulation de physique
    */
    
    physObj->speed.x += (force.x/mass)*(((double) deltaTime)*1e-3);
    physObj->speed.y += (force.y/mass)*(((double) deltaTime)*1e-3);
    
}

/**
 *  Cette fonction est appelée quand l'objet 1 et l'objet 2 sont en collision
 */
void handleCollision(PhysObject *object1, Vector normal2) {

    Vector *speed1 = &(object1->speed);

    /*      
      _X : Vecteur X
      <_X, _Y> : Produit scalaire entre _X et _Y
      ||X|| : Norme de X

      _N : Vecteur normal à la surface de l'objet 1
      _AC : Vecteur vitesse initial de l'objet 2
      _CA' : Vecteur vitesse final de l'objet 2
      I : Point tel que _CI est colinéaire à _N et _AI est orthogonal à _N

      On a _CA' = _CA + _AI + _IA' = _CA + 2*_AI (car _AI = _IA')

      Or            _AI = _AC + _CI      = _AC + ||_CI||*_N (car _N est de norme 1)
      De plus <_CA, _N> = ||_CI||*||_N|| = ||_CI||          (car _N est de norme 1)
      
      Donc _AI = _AC + <_CA, _N>*_N

      Et, finalement _CA' = -_AC + 2*(_AC + <_CA, _N>*_N)
                          = _AC + 2*<_CA, _N>*_N
     */
    
    // = <_CA, _N> = <-_AC, _N>
    const double dotProd1 = normal2.x * (-speed1->x) + normal2.y * (-speed1->y);

    // N := 2*<_CA, _N>*_N
    normal2.x *= 2*dotProd1;
    normal2.y *= 2*dotProd1;

    // _CA' = _AC + N = _AC + 2*<_CA, _N>*_N
    speed1->x += normal2.x * object1->bounciness;
    speed1->y += normal2.y * object1->bounciness;

    // On arrête l'objet s'il est trop lent   
    if (fabs(speed1->x) < MIN_SPEED) {
        speed1->x = 0;
    }
    if (fabs(speed1->y) < MIN_SPEED) {
        speed1->y = 0;        
    }
}
