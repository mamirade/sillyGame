#ifndef TESTCOLLISION_H
#define TESTCOLLISION_H

#include "testCommon.h"

void testCollision(FILE *, Collider *, Collider *, Point *);

const int N = 100000;

void (*collisionSucceded)(FILE *, Collider *, Collider *, Point *) = doNothing;
void (*collisionFailed)(FILE *, Collider *, Collider *, Point *) = testCollision;

#endif /* TESTCOLLISION_H */
