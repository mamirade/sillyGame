import numpy as np
import matplotlib.pyplot as plt

from drawShapes import *

with open("../../../build/output", "r", encoding="utf-8") as f:

    fig, ax = plt.subplots()

    first = f.readline().strip()
    firstVal = list(map(float, f.readline().strip().split()))

    match first:
        case "Circle / Rectangle":
             drawRectangleStr(f.readline().strip().split())
             colObjDrawer = drawCircleStr
        case "Circle / Circle":
             drawCircleStr(f.readline().strip().split())
             colObjDrawer = drawCircleStr
        case "Rectangle / Rectangle":
             drawRectangleStr(f.readline().strip().split())
             colObjDrawer = drawRectangleStr
        case "Circle / Line":
             drawLineStr(f.readline().strip().split())
             colObjDrawer = drawCircleStr
        case "Rectangle / Line":
             drawLineStr(f.readline().strip().split())
             colObjDrawer = drawRectangleStr
        case "Line / Line":
             drawLineStr(f.readline().strip().split())
             colObjDrawer = drawLineStr
        case _:
             print(f"TestCollisionPoint - Inconnu : {first}")
             
    points = [list(map(float, line.strip().split())) for line in f]
    # Peut planter si on a aucun point (dans quel cas le graphe n'est pas bien interessant de toute façon)
    if points == []:
        print("No points")
    else:    
    
        x, y, colX, colY = zip(*points)

        for colObj in zip(x, y):
            colObjDrawer(list(colObj) + firstVal, "green")
        
        plt.axis("equal")

        ax.scatter(x, y, color="blue", label="Centre de la figure")
        ax.scatter(colX, colY, color="red", label="Points de collision")
        ax.legend()
        plt.show()
