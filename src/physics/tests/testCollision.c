#include "testCollision.h"

void testCollision(FILE *f , Collider *col1, Collider *col2, Point *center) {
    (void) col1;
    (void) center;
    fprintf(f, "%f %f\n", col2->shape.center->x, col2->shape.center->y);
}
