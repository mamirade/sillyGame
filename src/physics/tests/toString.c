#include "toString.h"

#include <stdio.h>
#include <string.h>

char buffer[BUFF_SIZE];

/**
 *  Permet l'affichage d'un collider
 */
const char *colliderInfo(const Collider *c) {

    char shapeType[8] = "";
    char shapeInfo[64] = "";

    switch (c->shape.type) {
    case CIRCLE:
        strncpy(shapeType, "CIRCLE", 8);
        snprintf(shapeInfo, 64, "radius: %.2f", c->shape.circle.radius);
        break;
    case RECT:
        strncpy(shapeType, "RECT", 8);
        snprintf(shapeInfo, 64, "dimensions: (%.2f, %.2f)",
                 c->shape.rectangle.distToCenter.x,
                 c->shape.rectangle.distToCenter.y);
        break;
    default:
        strncpy(shapeType, "???", 8);
        strncpy(shapeInfo, "No info", 64);
        break;
    }
    
    snprintf(buffer, BUFF_SIZE,
             "Center : (%f, %f)\n\
              Shape (%s):\n\
              \t%s\n", c->shape.center->x, c->shape.center->y, shapeType, shapeInfo);

    return buffer;
}
