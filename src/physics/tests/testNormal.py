import numpy as np
import matplotlib.pyplot as plt

from drawShapes import *

with open("../../../build/output", "r", encoding="utf-8") as f:

    fig, ax = plt.subplots()

    first = f.readline().strip()
    firstVal = list(map(float, f.readline().strip().split()))

    match first:
        case "Circle / Rectangle":
             drawRectangle(f.readline().strip().split())
             colObjDrawer = drawCircleStr
        case "Circle / Circle":
             drawCircle(f.readline().strip().split())
             colObjDrawer = drawCircleStr
        case "Rectangle / Rectangle":
             drawRectangle(f.readline().strip().split())
             colObjDrawer = drawRectangleStr
        case "Circle / Line":
             drawLineStr(f.readline().strip().split())
             colObjDrawer = drawCircleStr
        case "Rectangle / Line":
             drawLineStr(f.readline().strip().split())
             colObjDrawer = drawRectangleStr
        case "Line / Line":
             drawLineStr(f.readline().strip().split())
             colObjDrawer = drawLineStr
        case _:
             print(f"TestCollisionPoint - Inconnu : {first}")
             
    points = [list(map(float, line.strip().split())) for line in f]
    # Peut planter si on a aucun point (dans quel cas le graphe n'est pas bien interessant de toute façon
    if points == []:
        print("No points")
    else:    
    
        x, y, colX, colY, n1x, n1y, n2x, n2y = zip(*points)

        for colObj in zip(x, y):
            colObjDrawer(list(colObj) + firstVal, "green")
            
        # q1 est la normale du collider1
        q1 = ax.quiver(colX, colY, n1x, n1y
                       , color="red", angles="xy", label="Collisions de l'objet mobile sur l'objet immobile")
        # q2 est la normale du collider2
        q2 = ax.quiver(colX, colY, n2x, n2y
                       , color="yellow", angles="xy", label="Collisions de l'objet immobile sur l'objet mobile")
        
        plt.axis("equal")

        ax.scatter(x, y, color="blue", label="Centre de la figure")
        
        ax.legend()
        plt.show()
