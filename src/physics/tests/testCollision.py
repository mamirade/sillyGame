import numpy as np
import matplotlib.pyplot as plt

from drawShapes import *

with open("../../../build/output", "r", encoding="utf-8") as f:

    fig, ax = plt.subplots()

    first = f.readline().strip()
    f.readline()
    
    match first:
        case "Circle / Rectangle":
             drawRectangleStr(f.readline().strip().split())
        case "Circle / Circle":
             drawCircleStr(f.readline().strip().split())
        case "Rectangle / Rectangle":
             drawRectangleStr(f.readline().strip().split())
        case "Circle / HalfPlane":
             drawHalfPlaneStr(f.readline().strip().split())
        case "Rectangle / HalfPlane":
             drawHalfPlaneStr(f.readline().strip().split())
        case "HalfPlane / HalfPlane":
             drawHalfPlaneStr(f.readline().strip().split())
        case _:
             print(f"TestCollisionPoint - Inconnu : {first}")


             
    points = [list(map(float, halfPlane.strip().split())) for halfPlane in f]
    x, y = [pair[0] for pair in points], [pair[1] for pair in points]

    plt.axis("equal")
    ax.scatter(x, y)
    plt.show()

