import matplotlib.pyplot as plt

def drawRectangleStr(halfPlane, c="red"):

    dX, dY = float(halfPlane[2]), float(halfPlane[3])
    topLeftX, topLeftY = float(halfPlane[0]) - dX, float(halfPlane[1]) - dY
    drawRectangle(topLeftX, topLeftY, dX, dY)
    
def drawRectangle(topLeftX, topLeftY, dX, dY, c="red"):    
    plt.plot(
        [topLeftX,
         topLeftX+2*dX,
         topLeftX+2*dX,
         topLeftX,
         topLeftX],
        [topLeftY,
         topLeftY,
         topLeftY+2*dY,
         topLeftY+2*dY,
         topLeftY],        
        color=c
    )

def drawCircleStr(halfPlane, c="red"):
    x, y, radius = tuple(map(float, halfPlane))
    drawCircle(x, y, radius, c)
    
def drawCircle(x, y, radius, c="red"):
    circle = plt.Circle((x, y), radius)
    circle.set_edgecolor(c)
    circle.set_facecolor('white')
    plt.gca().add_patch(circle)

def drawHalfPlaneStr(halfPlane, c="red"):
    x0, y0, dx, dy = tuple(map(float, halfPlane))
    drawHalfPlane(x0, y0, dx, dy, c)

def drawHalfPlane(x0, y0, dx, dy, c="red"):
    plt.axline((x0, y0), (x0+dx, y0+dy), color=c)
