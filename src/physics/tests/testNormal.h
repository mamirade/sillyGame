#ifndef TESTNORMAL_H
#define TESTNORMAL_H

#include "testCommon.h"

const int N = 15;

void testNormal(FILE *f , Collider *col1, Collider *col2, Point *center);

void (*collisionSucceded)(FILE *, Collider *, Collider *, Point *) = testNormal;
void (*collisionFailed)(FILE *, Collider *, Collider *, Point *) = doNothing;

#endif /* TESTNORMAL_H */
