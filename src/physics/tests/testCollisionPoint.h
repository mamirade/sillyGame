#ifndef TESTCOLLISIONPOINT_H
#define TESTCOLLISIONPOINT_H

#include "testCommon.h"

const int N = 15;

void testCollisionPoint(FILE *f , Collider *col1, Collider *col2, Point *center);

void (*collisionSucceded)(FILE *, Collider *, Collider *, Point *) = testCollisionPoint;
void (*collisionFailed)(FILE *, Collider *, Collider *, Point *) = doNothing;

#endif /* TESTCOLLISIONPOINT_H */
