import matplotlib.pyplot as plt

with open("../../../build/output", "r", encoding="utf-8") as f:

     x, y, _, _ = zip(*[map(float, l.strip().split()) for l in f])

     plt.plot(x, y)
     plt.show()
