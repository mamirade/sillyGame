#include "testCommon.h"
#include "testNormal.h"
#include "../normal.h"

// (centre col1) (point de contact) (normale à collider 1) (normale à collider 2)
void testNormal(FILE *f , Collider *col1, Collider *col2, Point *center) {
    
    fprintf(f, "%f %f %f %f ", col2->shape.center->x, col2->shape.center->y, center->x, center->y);
    Vector normal1 = getNormal(col1, center);
    Vector normal2 = getNormal(col2, center);

    fprintf(f, "%f %f %f %f\n", normal1.x, normal1.y, normal2.x, normal2.y);
}
