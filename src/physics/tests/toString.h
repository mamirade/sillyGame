#ifndef TOSTRING_H
#define TOSTRING_H

#include <stdlib.h>
#include "../collision.h"

#define BUFF_SIZE 255

const char *colliderInfo(const Collider* c);

#endif /* TOSTRING_H */
