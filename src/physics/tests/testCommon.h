#ifndef TESTCOMMON_H
#define TESTCOMMON_H

#include <stdio.h>
#include "../collision.h"

extern const int N;
extern void (*collisionSucceded)(FILE *, Collider *, Collider *, Point *);
extern void (*collisionFailed)(FILE *, Collider *, Collider *, Point *);

void doNothing(FILE *, Collider *, Collider *, Point *);

double randDouble(double max);

void testLoop(FILE * f, const Shape s1, Shape s2);

void testRectangleRectangle(FILE *f);
void testCircleRectangle(FILE *f);
void testCircleCircle(FILE *f); 

#endif /* TESTCOMMON_H */
