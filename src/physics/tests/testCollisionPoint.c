#include "testCollisionPoint.h"

void testCollisionPoint(FILE *f , Collider *col1, Collider *col2, Point *center) {

    (void) col1;
    
    fprintf(f, "%f %f ", col2->shape.center->x, col2->shape.center->y);
    fprintf(f, "%f %f\n", center->x, center->y);
}
