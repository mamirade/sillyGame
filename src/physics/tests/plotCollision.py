import numpy as np
import matplotlib.pyplot as plt

from drawShapes import *

with open("../../../build/output", "r", encoding="utf-8") as f:    

    for line in f:

        line = line.strip().split()
        match line[0]:
            case "Circle":
                drawCircle(float(line[2]), float(line[3]), float(line[1]))
            case "Rectangle":
                drawRectangle(float(line[3]), float(line[4]), float(line[1]), float(line[2]))
            case _:
                print(f"Forme inconnue : {line[0]}")

                
    plt.axis("equal")
    plt.show()
