#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

#include "testCommon.h"
#include "../shape.h"
#include "toString.h"

#define R1_DIM {10, 5}
#define R2_DIM {2, 3}

#define C1_RAD 1
#define C2_RAD 2

#define DIR_VECT_1 {1, 2}
#define DIR_VECT_2 {3, 4}

#define RAND_RANGE 20

void doNothing(FILE *f, Collider *c1, Collider *c2, Point *p) {
    (void) f;
    (void) c1;
    (void) c2;
    (void) p;
}

/**
 * Génère un nombre flottant aléatoire entre -max/2 et max/2
 */
double randDouble(double max) {
    
    return ((double) (rand() - (RAND_MAX>>1)) / (double) RAND_MAX) * max;
}


void testLoop(FILE * f, const Shape s1, Shape s2) {

    Point colPoint = {0, 0};
    int i;

    Collider col1,
             col2;
    initCollider(&col1, s1);
    initCollider(&col2, s2);

    bool collide1,
         collide2;
    
    for (i = 0; i < N; ++i) {
        
        col2.shape.center->x = randDouble(RAND_RANGE);
        col2.shape.center->y = randDouble(RAND_RANGE);

        collide1 = col1.isColliding(&col1, &col2, &colPoint);
        collide2 = col2.isColliding(&col2, &col1, &colPoint);
        
        if (collide1 && collide2) {
            collisionSucceded(f, &col1, &col2, &colPoint);
        }
        else if (!collide1 && !collide2) {
            collisionFailed(f, &col1, &col2, &colPoint);
        }
        else {
            fprintf(stderr, "ERREUR: Collision non commutative :\n %s %s",
                    colliderInfo(&col1), colliderInfo(&col2));
            exit(-1);
        }
    }
}
/**
 *  Format:
 *  Circle / Circle\n
 *  [rayon du cercle 2]\n
 *  [centreCercle1X] [centreCercle1Y] [rayon1]\n
 *  ...[centreCercle2X] [centreCercle2Y] [collisionX] [collisionY]\n...
 */
void testCircleCircle(FILE *f) {

    Point c1 =  {1, 2};
    Point c2 = {-42, -42};
    
    fputs("Circle / Circle\n", f);
    Shape s1 = initCircleShape(C1_RAD, &c1),
          s2 = initCircleShape(C2_RAD, &c2);

    fprintf(f, "%f\n", s2.circle.radius); 
    fprintf(f, "%f %f %f\n", s1.center->x, s1.center->y, s1.circle.radius);
    testLoop(f, s1, s2);
    
}

/**
 *  Note: on fait bouger le cercle et non le rectangle.
 *  Format:
 *  Circle / Rectangle\n
 *  [rayon du cercle]\n
 *  [centreRectX] [centreRectY] [dimRectX] [dimRectY]\n
 *  ...[centreCercleX] [centreCercleY] [collisionX] [collisionY]\n...
 */
void testCircleRectangle(FILE *f) {
    
    fputs("Circle / Rectangle\n", f);
    Vector dim = R1_DIM;

    Point rectC = {-2, -2};
    Point circC = {-42, -42};
    
    Shape rect = initRectangleShape(dim, &rectC),
          circ = initCircleShape(C1_RAD, &circC); // Ecrasé dans testLoop
    
    fprintf(f, "%f\n", circ.circle.radius);
    fprintf(f, "%f %f %f %f\n", rect.center->x, rect.center->y, rect.rectangle.distToCenter.x, rect.rectangle.distToCenter.y);

    testLoop(f, rect, circ);
    
}

/**
 *  Format:
 *  Rectangle / Rectangle\n
 *  [dimRect2X] [dimRect2Y]\n
 *  [centreRect1X] [centreRect1Y] [dimRect1X] [dimRect1Y]\n
 *  ...[centreRect2X] [centreRect2Y] [collisionX] [collisionY]\n...
 */
void testRectangleRectangle(FILE *f) {

    fputs("Rectangle / Rectangle\n", f);
    Vector dim1 = R1_DIM,
           dim2 = R2_DIM;

    Point c1 = {1, 1};
    Point c2 = {-42, -42};
    
    Shape rect1 = initRectangleShape(dim1, &c1),
        rect2 = initRectangleShape(dim2, &c2); // Ecrasé dans testLoop


    fprintf(f, "%f %f\n", rect2.rectangle.distToCenter.x, rect2.rectangle.distToCenter.y);
    fprintf(f, "%f %f %f %f\n", rect1.center->x, rect1.center->y, rect1.rectangle.distToCenter.x, rect1.rectangle.distToCenter.y);

    testLoop(f, rect1, rect2);    
}

void testCircleHalfPlane(FILE *f) {
    fputs("Circle / HalfPlane\n", f);

    // Le centre du cercle sera écrasé
    Point circC = {-42, -42};
    Point halfPlaneC = {1, 1};
    
    Shape circle = initCircleShape(C1_RAD, &circC);
    Shape halfPlane = initHalfPlaneShape((Vector) DIR_VECT_1, &halfPlaneC);
    
    fprintf(f, "%f\n", circle.circle.radius);
    fprintf(f, "%f %f %f %f\n",
            halfPlane.center->x, halfPlane.center->y,
            halfPlane.halfPlane.dirVector.x, halfPlane.halfPlane.dirVector.y);

    testLoop(f, halfPlane, circle);
}

void testRectangleHalfPlane(FILE *f) {
    fputs("Rectangle / HalfPlane\n", f);

    // Le centre du rectangle sera écrasé
    Point rectC = {-42, -42};
    Point halfPlaneC = {1, 1};
    
    Shape rect = initRectangleShape((Vector) R1_DIM, &rectC);
    Shape halfPlane = initHalfPlaneShape((Vector) DIR_VECT_1, &halfPlaneC);
    
    fprintf(f, "%f %f\n", rect.rectangle.distToCenter.x, rect.rectangle.distToCenter.y);
    fprintf(f, "%f %f %f %f\n",
            halfPlane.center->x, halfPlane.center->y,
            halfPlane.halfPlane.dirVector.x, halfPlane.halfPlane.dirVector.y);

    testLoop(f, halfPlane, rect);
}

void testHalfPlaneHalfPlane(FILE *f) {
    fputs("HalfPlane / HalfPlane\n", f);

    // Le centre du cercle sera écrasé
    Point halfPlane1C = {-42, -42};
    Point halfPlane2C = {1, 1};
    
    Shape halfPlane1 = initHalfPlaneShape((Vector) DIR_VECT_1, &halfPlane1C);
    Shape halfPlane2 = initHalfPlaneShape((Vector) DIR_VECT_2, &halfPlane2C);
    
    fprintf(f, "%f %f\n",
            halfPlane1.halfPlane.dirVector.x, halfPlane1.halfPlane.dirVector.y);
    fprintf(f, "%f %f %f %f\n",
            halfPlane2.center->x, halfPlane2.center->y,
            halfPlane2.halfPlane.dirVector.x, halfPlane2.halfPlane.dirVector.y);

    testLoop(f, halfPlane1, halfPlane2);
}

int main(int argc, char *argv[]) {
    
    int seed;

    if (argc <=1) {
        fputs("Pas le bon nombre d'arguments\n", stderr);
        return EXIT_FAILURE;
    }
    
    if (argc > 2) {
        seed = atoi(argv[2]);
    }
    else {
        seed = time(NULL);
    }

    printf("SEED: %d\n", seed);
    srand(seed);
    FILE *f = fopen("../../../build/output", "w");
    if (f == NULL) {
        perror("");
        return EXIT_FAILURE;
    }        

    int i = 0;
    void (*testerFunctions[])(FILE *f) = {testCircleCircle, testCircleRectangle, testRectangleRectangle, testCircleHalfPlane, testRectangleHalfPlane, testHalfPlaneHalfPlane};
    char *tests[]= {"CC", "CR", "RR", "CL", "RL", "LL", NULL};
    
    while (tests[i] != NULL) {
        if (strcmp(tests[i], argv[1]) == 0) {
            testerFunctions[i](f);
            break;
        }
        i++;
    }

    fclose(f);
    return 0;
}
