#ifndef PHYSICS_H
#define PHYSICS_H

#include <stdint.h>

#include "../dataStructures/list.h"
#include "../dataStructures/memoryPool.h"

#include "physicsCalc.h"

PhysObject initPhysObject(double mass, double bounciness, Shape s, physOptions options);

// La fonction est là pour ne pas avoir à dépendre de GameObject pour récupérer ce qui nous intéresse: le physObject associé
void physicsUpdate(list *gameObjects, memoryPool *gameObjPool, memoryPool *physObjPool,
                   const milliSeconds deltatime,
                   PhysObject *(*getPhysObject)(const void *),
                   void (*onCollision)(void *, void *));

#endif /* PHYSICS_H */
