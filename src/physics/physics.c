#include <assert.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include "normal.h"
#include "overlap.h"
#include "physics.h"

#define MIN_SPEED 0.1
#define MAX_INACTIVE_FRAMES 6

static void updatePosition(PhysObject *physObj, const milliSeconds deltaTime);

PhysObject initPhysObject(double mass, double bounciness, Shape s, physOptions options) {
    
    PhysObject obj = {
        .mass = mass,
        .bounciness = bounciness,
        .info.options = options,
    };

    // Un objet qui ne bouge pas aura une masse infinie
    // Ceci réduit grandement les cas particuliers à gérer pour les objets NO_FORCES
    if (options & NO_FORCES) {
        obj.mass = INFINITY;
    }
        
    initCollider(&obj.collider, s);
    obj.resolveOverlap = affectOverlapResolver(&obj);
    
    return obj;
}

/**
 *  Déplace un objet à sa position après un deltaTime.
 */
static void updatePosition(PhysObject *physObj, const milliSeconds deltaTime) {

    Point newCenter = *physObj->collider.shape.center;

    if (fabs(physObj->speed.x) > MIN_SPEED) {    
        newCenter.x += physObj->speed.x*((double) deltaTime)*1e-3;
    }
    if (fabs(physObj->speed.y) > MIN_SPEED) {
        newCenter.y += physObj->speed.y*((double) deltaTime)*1e-3;
    }

    *physObj->collider.shape.center = newCenter;
}

/**
 *  Prend en argument une liste de gameObjects (supposés avec un physObject attaché) 
 *  et le nombre de millisecondes depuis le dernier appel de la fonction.
 *  
 *  S'occupe d'effectuer une frame de la simulation de physique 
 *  (gestions des forces, des collisions, du déclenchement d'évenements et de la mise à jour de positions).
 */
void physicsUpdate(list *gameObjects, memoryPool *gameObjPool, memoryPool *physObjPool,
                   const milliSeconds deltaTime,
                   PhysObject *(*getPhysObject)(const void *),
                   void (*onCollision)(void *, void *)) {
    
    size_t i,
           j;
    PhysObject *obj = NULL;
    PhysObject *other = NULL;
    Vector collisionPoint = {};
    Vector n1 = {};
    Vector n2 = {};

    // Vaut toujours le nombre d'éléments enlevés dans la liste
    int offset = 0;
    
    for (i = 0; i < gameObjects->nbElem; ++i) {
        
        obj = getPhysObject(gameObjects->elems[i]);

        if (obj->info.state & MARKED_FOR_DELETE) {
            returnToMemoryPool(gameObjPool, gameObjects->elems[i]);
            returnToMemoryPool(physObjPool, obj);
            offset++;
            continue;
        }
        
        applyForces(obj, deltaTime);
        updatePosition(obj, deltaTime);
        
        for (j = i+1; j < gameObjects->nbElem; ++j) {
            other = getPhysObject(gameObjects->elems[j]);
            if (obj->collider.isColliding(
                    &obj->collider, &other->collider, &collisionPoint)) {
                
                n1 = getNormal(&obj->collider, &collisionPoint);
                n2 = getNormal(&other->collider, &collisionPoint);
                handleCollision(obj, n2);
                handleCollision(other, n1);
                
                updatePosition(obj, deltaTime);
                updatePosition(other, deltaTime);
                
                if (obj->collider.isColliding(
                        &obj->collider, &other->collider, NULL)) {
                    obj->resolveOverlap(obj, other, n1, n2, deltaTime);
                }
                
                onCollision(gameObjects->elems[i], gameObjects->elems[j]);
                onCollision(gameObjects->elems[j], gameObjects->elems[i]);
            }
        }
        
        // Décalage dans la liste pour écraser les éléments supprimés lors du passage de la boucle
        gameObjects->elems[i-offset] = gameObjects->elems[i];        
    }    
    gameObjects->nbElem -= offset;
}
