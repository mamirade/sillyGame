#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "overlap.h"
#include "../log.h"

#define EPS 1e-3
#define FORCE_DAMP 0.6

static void circleOverlapResolver(PhysObject *, PhysObject *, Vector n1, Vector n2, milliSeconds deltaTime);
static void rectangleOverlapResolver(PhysObject *, PhysObject *, Vector n1, Vector n2, milliSeconds deltaTime);
static void halfPlaneOverlapResolver(PhysObject *, PhysObject *, Vector n1, Vector n2, milliSeconds deltaTime);

/**
 *  On note
 *  	X_  : le vecteur X
 *  	|| X_ || : norme de X
 *  	X_' : le vecteur X à l'itération suivante
 *  	X_x : la composante x du vecteur X
 *  	X_y : la composante y du vecteur Y
 *  	dt  : deltaTime
 *
 *  On cherche à appliquer une force F1_ à obj et une force F2_ à other telle que:
 *  	|| F1_ || = || F2_ ||
 *  	F1_ = k*n2_ et F2_ = k*n1_ avec n1_, n2_ les normes de l'objet 1 et 2 et k > 0
 *  	|| p1_' - p2_' || = R avec R = 0
 */
static void separatePoints(PhysObject *obj, PhysObject *other,
                    Vector n1, Vector n2, milliSeconds deltaTime,
                    const Point *p1, const Point *p2, double R) {

    double dtseconds = ((double) deltaTime)*1e-3;
    
    const Vector *v1 = &obj->speed;
    const Vector *v2 = &other->speed;

    /*
     *  On a F1_ = m1*a1_ = m1 * (v1_' - v1_)/dt (a1_: accélération de l'objet 1) 
     *      => v1_' = (dt/m1)*F1_ + v1_
     *  Et, de manière symétrique, 
     *         v2_' = (dt/m2)*F2_ + v2_
     *
     *  On a:
     *      c1_' = c1_ + dt*v1_' = c1_ + dt*((dt/m1)F1_ + v1_) = (dt²/m1)F1_ + dt*v1_ + c1_
     *  Symétriquement: 
     *      c2_' = (dt²/m2)F2_ + dt*v2_ + c2_
     *
     *  	|| c1_' - c2_' || = R  =>
     *  	|| (dt²/m1)F1_ + dt*v1_ + c1_ - (dt²/m2)F2_ - dt*v2_ - c2_ || = R
     *
     *  En remplaçant F1_ par k*n2_, F2_ par k*n1_ et en factorisant:
     *      || dt²*k*(n2_/m1 - n1_/m2) + dt(v1_ - v2_) + c1_ - c2_ || = R
     *
     *  En posant:
     */

    const double Nx = dtseconds*dtseconds*(n2.x/obj->mass - n1.x/other->mass);
    const double Ny = dtseconds*dtseconds*(n2.y/obj->mass - n1.y/other->mass);
    const double Cx = dtseconds*(v1->x - v2->x) + p1->x - p2->x;
    const double Cy = dtseconds*(v1->y - v2->y) + p1->y - p2->y;


    /*
     *  On obtient:
     *      (k*Nx + Cx)² + (k*Ny + Cy)² = R²
     *      (k*Nx)² + 2*k*Nx*Cx + Cx² + (k*Ny)² + 2*k*Ny*Cy + Cy² = R²
     *      (Nx² + Ny²)*k² + 2*(Nx*Cx + Ny*Cy)*k + (Cx² + Cy² - R²) = 0
     *
     *  Ce qui nous donne (en notant d le discriminant de cette équation du 2nd degré):
     *      d = (2*(Nx*Cx + Ny*Cy))² - 4(Nx² + Ny²)(Cx² + Cy² - R²)
     *      d = 4(Nx*Cx + Ny*Cy)² - (Nx² + Ny²)(Cx² + Cy² - R²))
            d = 4b² - a(Cx² + Cy² - R²) avec les notations ci-dessous
     *
     *  Le 4 se simplifie avec le 2 du dénominateur et le 2 devant le coefficient du terme de 1er degré
     */

    const double a = Nx*Nx + Ny*Ny;
    const double b = Nx*Cx + Ny*Cy;
    double delta = b*b - a*(Cx*Cx + Cy*Cy - R*R);
    if (delta >= 0) {
    
        delta = sqrt(delta);
    
        // Donc, puisque k > 0, on a:
        const double k = (delta > b)
            ? (-b+delta)/a
            : (-b-delta)/a;
    
        if (k >= 0) {    
            // D'où, finalement
            const Vector F1 = {
                FORCE_DAMP*k*n2.x,
                FORCE_DAMP*k*n2.y,
            };
            applyForceToPhysObject(obj, F1, deltaTime);

            const Vector F2 = {
                FORCE_DAMP*k*n1.x,
                FORCE_DAMP*k*n1.y,
            };
            applyForceToPhysObject(other, F2, deltaTime);
        }
    }
}

static void circleCircleOverlap(PhysObject *circ1, PhysObject *circ2, Vector n1, Vector n2, milliSeconds deltaTime) {

    /*
     *  On cherche à éloigner les centres des cercles le long de la normale de 
     *  l'objet opposant de telle sorte à ce qu'il ne se touchent plus au prochain deltaTime. 
     *  Leurs centres seront à une distance égale à la somme de leur rayon,
     *  ils ne se toucheront donc plus.
     */    
    separatePoints(circ1, circ2, n1, n2, deltaTime,
                   circ1->collider.shape.center,
                   circ2->collider.shape.center,
                   circ1->collider.shape.circle.radius + circ2->collider.shape.circle.radius);    
}

static void circleHalfPlaneOverlap(PhysObject *circ, PhysObject *halfPlane, Vector n1, Vector n2, milliSeconds deltaTime) {

    const double x0 = halfPlane->collider.shape.center->x;
    const double y0 = halfPlane->collider.shape.center->y;
    const double dx = halfPlane->collider.shape.halfPlane.dirVector.x;
    const double dy = halfPlane->collider.shape.halfPlane.dirVector.y;

    const double cx = circ->collider.shape.center->x;
    const double cy = circ->collider.shape.center->y;
    
    /*
     *  On cherche le point P tel que P est le projeté orthogonal du centre de 
     *  obj sur la demi-plan other.
     *
     *  Ce point P vérifie xp = x0 + t*dx
     *                     yp = y0 + t*dy
     *  Et, puisque P est le projeté orthogonal de C sur halfPlane, on a que le produit 
     *  scalaire entre le vecteur CP et dirVector est nul.
     *  i.e:  dx(xp - xc) + dy(yp - yc) = 0 <=> dx(x0 + t*dx - xc) + dy(y0 + t*dy - yc) = 0
     * 
     *  Ce qui donne comme solution pour t:
    */
    const double t = (dx*(cx - x0) + dy*(cy - y0))/(dx*dx + dy*dy);

    // Donc, P vaut:
    const Point P = (Point) {
        x0 + t*dx,
        y0 + t*dy,
    };
    
    separatePoints(circ, halfPlane, n1, n2, deltaTime,
                   circ->collider.shape.center,
                   &P,
                   circ->collider.shape.circle.radius);
}

static void circleRectangleOverlap(PhysObject *circ, PhysObject *rect, Vector n1, Vector n2, milliSeconds deltaTime) {
    (void) circ;
    (void) rect;
    (void) n1;
    (void) n2;
    (void) deltaTime;
    LOG_BUG("Do your job");
}

static void rectangleRectangleOverlap(PhysObject *rect1, PhysObject *rect2, Vector n1, Vector n2, milliSeconds deltaTime) {
    (void) rect1;
    (void) rect2;
    (void) n1;
    (void) n2;
    (void) deltaTime;
    LOG_BUG("Do your job");
}

static void rectangleHalfPlaneOverlap(PhysObject *rect, PhysObject *halfPlane, Vector n1, Vector n2, milliSeconds deltaTime) {
    (void) rect;
    (void) halfPlane;
    (void) n1;
    (void) n2;
    (void) deltaTime;
    LOG_BUG("Do your job");
}

void (*affectOverlapResolver(PhysObject *obj))(PhysObject *, PhysObject *, Vector, Vector, milliSeconds deltaTime) {
 
    switch (obj->collider.shape.type) {
    case CIRCLE:        
        return circleOverlapResolver;
    case RECT:
        return rectangleOverlapResolver;
    case HALF_PLANE:
        return halfPlaneOverlapResolver;
    default:
        LOG_BUG("UNKNOWN SHAPE FOUND");
        exit(1);
    }
}

static void circleOverlapResolver(PhysObject *obj, PhysObject *other, Vector n1, Vector n2, milliSeconds deltaTime) {
    switch (other->collider.shape.type) {
    case CIRCLE:
        circleCircleOverlap(obj, other, n1, n2, deltaTime);
        break;
    case RECT:
        circleRectangleOverlap(obj, other, n1, n2, deltaTime);
        break;
    case HALF_PLANE:
        circleHalfPlaneOverlap(obj, other, n1, n2, deltaTime);
        break;
    default:
        LOG_BUG("UNKNOWN SHAPE FOUND");
        exit(1);
    }
}

static void rectangleOverlapResolver(PhysObject *obj, PhysObject *other, Vector n1, Vector n2, milliSeconds deltaTime) {
    switch (other->collider.shape.type) {
    case CIRCLE:
        circleRectangleOverlap(other, obj, n2, n1, deltaTime);
        break;
    case RECT:
        rectangleRectangleOverlap(obj, other, n1, n2, deltaTime);
        break;
    case HALF_PLANE:
        rectangleHalfPlaneOverlap(obj, other, n1, n2, deltaTime);
        break;
    default:
        LOG_BUG("UNKNOWN SHAPE FOUND");
        exit(1);
    }
}

static void halfPlaneOverlapResolver(PhysObject *obj, PhysObject *other, Vector n1, Vector n2, milliSeconds deltaTime) {
    switch (other->collider.shape.type) {
    case CIRCLE:
        circleHalfPlaneOverlap(other, obj, n2, n1, deltaTime);
        break;
    case RECT:
        rectangleHalfPlaneOverlap(other, obj, n2, n1, deltaTime);
        break;
    case HALF_PLANE: // à moins qu'elles soient parrallèles, rien n'est possible ici
        break;
    default:
        LOG_BUG("UNKNOWN SHAPE FOUND");
        exit(1);
    }
}
