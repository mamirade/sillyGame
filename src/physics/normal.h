#ifndef NORMAL_H
#define NORMAL_H

#include "collision.h"

// Les normales ne dépendent pas de la forme "entrante"
Vector getNormal(const Collider *col, const Point *collisionPoint);

Vector circleNormal(const Shape *circ, const Point *collisionPoint);
Vector rectNormal(const Shape *rect, const Point *collisionPoint);
Vector halfPlaneNormal(const Shape *halfPlane, const Point *collisionPoint);

#endif /* NORMAL_H */
