#ifndef ELEMENTARY_H
#define ELEMENTARY_H

typedef struct {
    double x;
    double y;
} Vector;

typedef Vector Point;

typedef enum shape_type {
    CIRCLE,
    RECT,
    HALF_PLANE
} shape_type;

/**
 * @field radius Un cercle est entièrement décrit par son rayon
 */
typedef struct {
    double radius;
} Circle;

/**
 * @field distToCenter (x, y) tel que x et y sont les distances horizontales
 * et verticales du centre à un bord du rectangle
 */
typedef struct {
    Vector distToCenter;
} Rectangle;

/**
 *  @field dirVector (x, y) un vecteur directeur de la droite. Celui-ci sera toujours supposé de norme 1
 */
typedef struct {
    Vector dirVector;
} HalfPlane;

/**
 *  @field center Centre du cercle circonscrit. Le centre est un pointeur
 *  car le centre de l'objet est utilisé dans d'autres contextes que sa forme
 *  et que dans tous ces contextes, les centres doivent pointer le même centre.
 */
typedef struct {
    Point *center;
    shape_type type;
    union {
        Circle circle;
        Rectangle rectangle;
        HalfPlane halfPlane;
    };
} Shape;

Shape initCircleShape(double radius, Point *center);
Shape initRectangleShape(Vector dimensions, Point *center);
// Le vecteur directeur sera normé à 1 par la fonction
Shape initHalfPlaneShape(Vector dirVector, Point *center);
#endif /* ELEMENTARY_H */
