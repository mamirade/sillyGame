#ifndef OVERLAP_H
#define OVERLAP_H

#include "physicsCalc.h"

void (*affectOverlapResolver(PhysObject *))(PhysObject *, PhysObject *, Vector n1, Vector n2, milliSeconds deltaTime);
#endif /* OVERLAP_H */
