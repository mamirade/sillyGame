#include <math.h>
#include "shape.h"

Shape initCircleShape(double radius, Point *center) {

    Shape s;
    s.center = center;
    s.type = CIRCLE;
    s.circle.radius = radius;
    return s;
}

Shape initRectangleShape(Vector dim, Point *center) {

    Shape s;
    s.center = center;
    s.type = RECT;
    s.rectangle.distToCenter = dim;

    // Dans nos calculs, les distances au centres sont plus intéressantes
    // Mais les dimensions sont plus intuitives (à mon avis)
    s.rectangle.distToCenter.x /= 2;
    s.rectangle.distToCenter.y /= 2;
    
    return s;
}

Shape initHalfPlaneShape(Vector dirVector, Point *center) {
    Shape s;
    s.center = center;
    s.type = HALF_PLANE;
    s.halfPlane.dirVector = dirVector;

    const double norm = hypot(dirVector.x, dirVector.y);

    s.halfPlane.dirVector.x /= norm;
    s.halfPlane.dirVector.y /= norm;
    
    return s;
}
