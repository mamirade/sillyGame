#include "log.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>

#define BUFF_SIZE 128
#define LOG_FILE_NAME "error_log.txt"

FILE *logFile = NULL;

static void closeLogAtExit(void);
static void sigCloseLog(int);

/**
 *  Ouvre le fichier de Log et s'assure que celui-ci sera fermé en
 *  cas de terminaison normale du programme
 */
bool beginLog(void) {

    logFile = fopen(LOG_FILE_NAME, "a");
    if (logFile == NULL) {
        perror("Could not open log file");
        return false;
    }

    if (atexit(closeLogAtExit) != 0) {
        perror("Could not assure closing of log file");
        fclose(logFile);
        return false;
    }
    // Même en cas de SIGABRT, on veut que le fichier soit fermé correctement
    signal(SIGABRT, sigCloseLog);
    
    return true;
}

/**
 *  Fonciton appelée par atexit.
 */
static void closeLogAtExit(void) {
    if (logFile != NULL) {
        fclose(logFile);
        logFile = NULL;
    }
}

static void sigCloseLog(int code) {
    (void) code;
    exit(EXIT_FAILURE);
}

/**
 *  Fonction appelée par les différentes macros LOG_ERROR, LOG_BUG et LOG_INFO
 */
int _log(const char *message, const char *function_name, const char *file_name, int line, const char *messageType) {
    
    char timeBuffer[BUFF_SIZE];
    time_t current = time(NULL);

    strftime(timeBuffer, BUFF_SIZE, "%H:%M:%S", localtime(&current));
    fprintf(logFile, "%s | [%s]: %s (at %s in %s:%d)\n",
            messageType, timeBuffer, message, function_name, file_name, line);

    return EXIT_SUCCESS;
}


