#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdbool.h>
#include <SDL2/SDL.h>

#include "../dataStructures/list.h"

#include "../physics/shape.h"

#define NUMBER_OF_SEALS 10

typedef struct graphicsInfo graphicsInfo;

graphicsInfo *initGraphics(SDL_Renderer *rend, int w, int h, char spriteFolder[]);
void endGraphics(graphicsInfo *info);

Point asGameSpacePoint(const graphicsInfo *info, const Point *screenSpaceP);
Point asScreenSpacePoint(const graphicsInfo *info, const Point *gameSpaceP);

SDL_Texture *getSealSprite(unsigned size);
void getWalls(SDL_Texture **leftWall, SDL_Texture **rightWall, SDL_Texture **ground);
int getPixelsPerMeter(const graphicsInfo *info);


void getGameWindowSize(const graphicsInfo *info, int *w, int *h);                  
void setWindowSize(graphicsInfo *info, int newWindowW, int newWindowH);

void rendererAddSprite(const graphicsInfo *info, SDL_Renderer *rend, SDL_Texture *sprite, const Point *center);
void graphicsUpdate(const graphicsInfo *info, SDL_Renderer *rend, const list *gameObjects, SDL_Texture *(*getSprite)(const void *, Point *));

#endif /* GRAPHICS_H */
