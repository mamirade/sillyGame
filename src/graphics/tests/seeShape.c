#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../../log.h"
#include "../graphics.h"
#include "../../gameLogic/gameObject.h"

bool initSDL(SDL_Window **w, SDL_Renderer **r) {

    bool res = true;
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        goto sdl_error;
    }
    if (IMG_Init(IMG_INIT_JPG) == 0) {
        goto img_error;
    }

    *w = SDL_CreateWindow("Sealy game",
                          SDL_WINDOWPOS_CENTERED,
                          SDL_WINDOWPOS_CENTERED,
                          1280,
                          960,
                          SDL_WINDOW_RESIZABLE);
    if (*w == NULL) {
        goto window_error;
    }
    *r = SDL_CreateRenderer(*w, -1, SDL_RENDERER_ACCELERATED);
    if (*r == NULL) {
        goto renderer_error;
    }

    SDL_SetRenderDrawColor(*r, 255,255,255, SDL_ALPHA_OPAQUE);
    
    if (0) {
    renderer_error:
        SDL_DestroyWindow(*w);
    window_error:
        IMG_Quit();
    img_error:
        SDL_Quit();
    sdl_error:
        res = false;
    }
    return res;
}

void drawCircle(SDL_Renderer *rend, int circCenterX, int circCenterY, double radius) {

    int radius2 = radius*radius;

    for (int x = (int) -radius; x < (int) radius; ++x) {
        for (int y = (int) -radius; y < (int) radius; ++y) {
            if (x*x + y*y < radius2) {
                SDL_RenderDrawPoint(rend, circCenterX+x, circCenterY+y);
            }
        }
    }
}

int main() {

    SDL_Renderer *rend;
    SDL_Window *window;

    assert(beginLog());
    assert(initSDL(&window, &rend));
    assert(initGraphics(rend, "../../../resources/sprites/"));
    
    GameObject obj[NUMBER_OF_SEALS];
    PhysObject physObjs[NUMBER_OF_SEALS];
    for (int i = 0; i < NUMBER_OF_SEALS; ++i) {
        initSeal(obj+i, physObjs+i, i);
    }

    SDL_RenderClear(rend);
    for (int i = 0; i < NUMBER_OF_SEALS; ++i) {
        obj[i].center.x = i*100+100;
        obj[i].center.y = 480;

        SDL_SetRenderDrawColor(rend, 255, 0, 0, SDL_ALPHA_OPAQUE);
        drawCircle(rend,
                   obj[i].center.x, obj[i].center.y,
                   obj[i].physObject->collider.shape.circle.radius*PIXELS_PER_METER);
        SDL_SetRenderDrawColor(rend, 255, 255, 255, SDL_ALPHA_OPAQUE);
        asGameSpacePoint(&obj[i].center);
        rendererAddGameObject(rend, obj+i);                   
    }
    SDL_RenderPresent(rend);

    SDL_Event e;
    while (SDL_PollEvent(&e) || true) {
        if (e.type == SDL_QUIT ||
            (e.type == SDL_WINDOWEVENT &&
             e.window.event == SDL_WINDOWEVENT_CLOSE)) {
            return 0;
        }
    }
}
