#include <SDL2/SDL_image.h>

#include "graphics.h"
#include "../log.h"

#define INIT_PIXELS_PER_METER 128

static SDL_Texture *seals[NUMBER_OF_SEALS];
static SDL_Texture *leftWall;
static SDL_Texture *rightWall;
static SDL_Texture *ground;

typedef struct graphicsInfo {
    double aspectRatio;
    double scaling;
    int gameWindowWidth;
    int gameWindowHeight;
    int leftPadding;
    int upperPadding;    
} graphicsInfo;

Point asGameSpacePoint(const graphicsInfo *info, const Point *screenSpaceP) {
    return (Point) {
        (screenSpaceP->x - info->leftPadding) /getPixelsPerMeter(info),
        // L'abscisse du repère en mètre va de bas en haut, alors que le repère de la fenêtre de haut en bas
        // D'où le signe moins
        (-screenSpaceP->y - info->upperPadding)/getPixelsPerMeter(info),
    };
}

Point asScreenSpacePoint(const graphicsInfo *info, const Point *gameSpaceP) {
    return (Point) {
        gameSpaceP->x*getPixelsPerMeter(info) + info->leftPadding,
        // L'abscisse du repère en mètre va de bas en haut, alors que le repère de la fenêtre de haut en bas
        // D'où le signe moins
        -gameSpaceP->y*getPixelsPerMeter(info) + info->upperPadding,
    };
}


/**
 *  Récupère le sprite correct pour un phoque de taille donné
 */
SDL_Texture *alloc_Seal(SDL_Renderer *rend, unsigned size, char spriteFolder[]) {

    SDL_Texture *sprite;
    char buffer[256];
    
    if (size >= NUMBER_OF_SEALS) {
        snprintf(buffer, 256, "Invalid seal size: %u", size);
        LOG_BUG(buffer);
        return NULL;
    }
    
    snprintf(buffer, 256, "%s/seal_%02u.png", spriteFolder, size);
    
    if ((sprite = IMG_LoadTexture(rend, buffer)) == NULL) {
        LOG_ERROR(SDL_GetError());
    }
    return sprite;
}

/**
 *  Libère la mémoire associé au phoque alloué par getSeal
 */
void free_Seal(SDL_Texture *seal) {
    SDL_DestroyTexture(seal);
}

bool initWalls(SDL_Renderer *rend, char spriteFolder[]) {

    char buffer[256];
    snprintf(buffer, 256, "%s/left_wall.png", spriteFolder);
    leftWall = IMG_LoadTexture(rend, buffer);
    if (leftWall == NULL) {
        LOG_ERROR(SDL_GetError());
        return false;
    }

    snprintf(buffer, 256, "%s/right_wall.png", spriteFolder);
    rightWall = IMG_LoadTexture(rend, buffer);
    if (rightWall == NULL) {
        LOG_ERROR(SDL_GetError());
        SDL_DestroyTexture(leftWall);
        leftWall = NULL;
        
        return false;
    }
    
    snprintf(buffer, 256, "%s/ground.png", spriteFolder);
    ground = IMG_LoadTexture(rend, buffer);
    if (ground == NULL) {
        LOG_ERROR(SDL_GetError());        
        SDL_DestroyTexture(leftWall);
        leftWall = NULL;
        SDL_DestroyTexture(rightWall);
        rightWall = NULL;
        
        return false;
    }
    return true;
}

graphicsInfo *initGraphics(SDL_Renderer *rend, int w, int h, char spriteFolder[]) {

    graphicsInfo *out = calloc(1, sizeof(graphicsInfo));
    if (out == NULL) {
        return NULL;
    }
    
    out->gameWindowWidth = w;
    out->gameWindowHeight = h;

    out->aspectRatio = ((double) w) / ((double) h);
    out->scaling = 1;

    out->leftPadding = 0;
    out->upperPadding = 0;
    
    if (!initWalls(rend, spriteFolder)) {
        free(out);
        return NULL;
    }
            
    int i;
    for (i = 0; i < NUMBER_OF_SEALS; ++i) {        
        seals[i] = alloc_Seal(rend, i, spriteFolder);
        if (seals[i] == NULL) {
            for (; i >= 0; --i) {
                free_Seal(seals[i]);
                seals[i] = NULL;               
            }
            SDL_DestroyTexture(leftWall);
            leftWall = NULL;
            SDL_DestroyTexture(rightWall);
            rightWall = NULL;
            SDL_DestroyTexture(ground);
            ground = NULL;
            free(out);
            return NULL;
        }
    }
    
    return out;
}

void endGraphics(graphicsInfo *info) {

    SDL_DestroyTexture(ground);
    SDL_DestroyTexture(rightWall);
    SDL_DestroyTexture(leftWall);
    int i;
    for (i = 0; i < NUMBER_OF_SEALS; ++i) {
        free_Seal(seals[i]);
        seals[i] = NULL;
    }
    free(info);
}

SDL_Texture *getSealSprite(unsigned size) {
    return seals[size];
}

void getWalls(SDL_Texture **leftWall_out, SDL_Texture **rightWall_out, SDL_Texture **ground_out) {

    if (leftWall_out != NULL) {
        *leftWall_out = leftWall;
    }
    if (rightWall_out != NULL) {
        *rightWall_out = rightWall;
    }
    if (ground_out != NULL) {
        *ground_out = ground;
    }
}

int getPixelsPerMeter(const graphicsInfo *info) {
    return (int) (((double) INIT_PIXELS_PER_METER)*info->scaling);
}

void getGameWindowSize(const graphicsInfo *info, int *gameWindowW, int *gameWindowH) {
    if (gameWindowW != NULL) {
        *gameWindowW = info->gameWindowWidth;
    }
    if (gameWindowH != NULL) {
        *gameWindowH = info->gameWindowHeight;
    }
}

void setWindowSize(graphicsInfo *info, int newWindowW, int newWindowH) {

    const double prevWindowW = (double) info->gameWindowWidth;
    
    if (((double) newWindowW) /((double) newWindowH) > info->aspectRatio) {
        
        info->gameWindowWidth = (int) (info->aspectRatio * ((double)newWindowH));
        info->gameWindowHeight = newWindowH;
            
        info->upperPadding = 0;
        info->leftPadding = (newWindowW - info->gameWindowWidth)/2;
    }
    else {        
        info->gameWindowWidth = newWindowW;
        info->gameWindowHeight = (int) (((double)newWindowW)/info->aspectRatio);
            
        info->upperPadding = (newWindowH - info->gameWindowHeight)/2;
        info->leftPadding = 0;
    }

    info->scaling *= (((double)info->gameWindowWidth)/prevWindowW);
}

SDL_Rect renderRectangle(const graphicsInfo *info, SDL_Texture *sprite, const Point *center) {

    int w;
    int h;

    SDL_QueryTexture(sprite,
                     NULL, NULL, &w, &h);

    w = ((double) w) * info->scaling;
    h = ((double) h) * info->scaling;

    Point p = asScreenSpacePoint(info, center);
    
    return (SDL_Rect) {
        // Nos coordonnées sont à partir du centre, il nous faut le bord haut droit
        p.x - w / 2,
        p.y - h / 2,
        w, h
    };
}

void rendererAddSprite(const graphicsInfo *info, SDL_Renderer *rend, SDL_Texture *sprite, const Point *center) {
    
    SDL_Rect destination = renderRectangle(info, sprite, center);

    SDL_RenderCopy(rend, sprite, NULL, &destination);
}


void graphicsUpdate(const graphicsInfo *info, SDL_Renderer *rend, const list *gameObjects, SDL_Texture *(*getSprite)(const void *, Point *)) {

    size_t i;
    Point center = {};
    SDL_Texture *texture = NULL;
    
    for (i = 0; i < gameObjects->nbElem; ++i) {
        texture = getSprite(gameObjects->elems[i], &center);
        rendererAddSprite(info, rend, texture, &center);
    }   
}
