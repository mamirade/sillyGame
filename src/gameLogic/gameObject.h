#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <SDL2/SDL.h>

#include "../dataStructures/memoryPool.h"

#include "../physics/shape.h"
#include "../physics/physics.h"
#include "../graphics/graphics.h"

#define RAND_SEAL_MAX_SIZE 3

typedef enum {
    SEAL_GAMEOBJECT,
    BORDER_GAMEOBJECT,
} gameObjType;

typedef struct {
    gameObjType type;
    Vector center;
    PhysObject *physObject;
    SDL_Texture *sprite;
    const void *additionalInfo;
} GameObject;

void initSeal(GameObject *seal, PhysObject *physObj, unsigned size);
GameObject *getRandomSeal(memoryPool *gameObjectPool, memoryPool *physObjectPool);
void onSealCollision(void *seal1, void *seal2);

void addBordersToList(const graphicsInfo *info, memoryPool *gameObjectPool, memoryPool *physObjectPool, list *activeObjectList);

PhysObject *getPhysObject(const GameObject *obj);
// Même fonction que celle sans _ préfixé, on cherche à avoir une signature
// compatible avec physicsUpdate
PhysObject *_getPhysObject(const void *obj);

SDL_Texture *getSprite(const GameObject *obj, Point *center_out);
// Même fonction que celle sans _ préfixé, on cherche à avoir une signature
// compatible avec graphicsUpdate
SDL_Texture *_getSprite(const void *obj, Point *center_out);
#endif /* GAMEOBJECT_H */
