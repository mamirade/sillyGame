#include "../log.h"
#include "gameObject.h"
#include "../graphics/graphics.h"

static const struct sealInfo {
    unsigned size;
} additionalInfos[NUMBER_OF_SEALS] = {{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}};

void onSealCollision(void *s1, void *s2) {
    GameObject *seal1 = (GameObject *) s1;
    GameObject *seal2 = (GameObject *) s2;

    if (seal1->type != SEAL_GAMEOBJECT || seal2->type != SEAL_GAMEOBJECT) {
        return;
    }

    // Vérification que cette fonction n'a pas déjà été appelée avec les paramètres
    // inversés ou sur un phoque pas encore supprimé
    if (!(seal1->physObject->info.state & MARKED_FOR_DELETE
       || seal2->physObject->info.state & MARKED_FOR_DELETE)
        // Puis, si les phoques ont la même taille
        && seal1->additionalInfo == seal2->additionalInfo) {

        if (((struct sealInfo*) seal1->additionalInfo)->size < 9) {
            initSeal(seal1, seal1->physObject, ((struct sealInfo *) seal1->additionalInfo)->size+1);
        }
        else {
            seal1->physObject->info.state |= MARKED_FOR_DELETE;
        }
        seal2->physObject->info.state |= MARKED_FOR_DELETE;
    }
}

void initSeal(GameObject *seal, PhysObject *physObj, unsigned size) {

    seal->type = SEAL_GAMEOBJECT;
    seal->physObject = physObj;
    seal->sprite = getSealSprite(size);
    seal->additionalInfo = additionalInfos+size;

    double mass = 0.7*(15 + size);
    
    *physObj = initPhysObject(mass, 0.8, initCircleShape(0.105 + ((double) size / 125), &seal->center), 0);
}

GameObject *getRandomSeal(memoryPool *gameObjectPool, memoryPool *physObjectPool) {
    
    GameObject *obj = getFromMemoryPool(gameObjectPool);    
    PhysObject *physObj = getFromMemoryPool(physObjectPool);
    if ((obj == NULL) || (physObj == NULL)) {
        LOG_ERROR("Out of Memory !");
        exit(1);
    }

    unsigned size = rand() % RAND_SEAL_MAX_SIZE;
    initSeal(obj, physObj, size);    
    
    return obj;
}

void initBorder(GameObject *border, memoryPool *physObjectPool, SDL_Texture *sprite, const Shape *borderLine) {
    PhysObject *physObj = getFromMemoryPool(physObjectPool);        
    border->physObject = physObj;
    border->type = BORDER_GAMEOBJECT;
    border->sprite = sprite;
    border->additionalInfo = NULL;

    *physObj = initPhysObject(INFINITY, 0.9, *borderLine, NO_FORCES);
}

void addBordersToList(const graphicsInfo *info,memoryPool *gameObjectPool, memoryPool *physObjectPool, list *gameObjects) {

    int windowWidth;
    int windowHeight;
    
    SDL_Texture *leftWallSprite  = NULL,
                *rightWallSprite = NULL,
                *groundSprite    = NULL;
    Shape line = {};
    Vector dir = {0, 1};
    
    getWalls(&leftWallSprite, &rightWallSprite, &groundSprite);
    getGameWindowSize(info, &windowWidth, &windowHeight);
    
    GameObject *leftWall = getFromMemoryPool(gameObjectPool);
    leftWall->center = (Vector) {
        0.25*((double)windowWidth),
        0.5*((double)windowHeight)
    };
    leftWall->center =
        asGameSpacePoint(info, &leftWall->center);
    line = initHalfPlaneShape(dir, &leftWall->center);
    initBorder(leftWall, physObjectPool, leftWallSprite, &line);
    addEndList(gameObjects, leftWall);

    GameObject *rightWall = getFromMemoryPool(gameObjectPool);
    rightWall->center = (Vector) {
        0.75*((double)windowWidth),
        0.5*((double)windowHeight)
    };
    rightWall->center =
        asGameSpacePoint(info, &rightWall->center);
    dir.y = -1;    
    line = initHalfPlaneShape(dir, &rightWall->center);
    initBorder(rightWall, physObjectPool, rightWallSprite, &line);
    addEndList(gameObjects, rightWall);
    
    GameObject *ground = getFromMemoryPool(gameObjectPool);
    ground->center = (Vector) {
        0.5*((double)windowWidth),
        0.75*((double)windowHeight)
    };
    ground->center =
        asGameSpacePoint(info, &ground->center);
    dir.x = -1;
    dir.y = 0;
    line = initHalfPlaneShape(dir, &ground->center);
    initBorder(ground, physObjectPool, groundSprite, &line);
    addEndList(gameObjects, ground);
}

PhysObject *getPhysObject(const GameObject *obj) {
    return obj->physObject;
}

PhysObject *_getPhysObject(const void *obj) {
    return ((GameObject *) obj)->physObject;
}

SDL_Texture *getSprite(const GameObject *obj, Point *center_out) {
    *center_out = obj->center;
    return obj->sprite;
}

SDL_Texture *_getSprite(const void *obj, Point *center_out) {
    *center_out = ((GameObject *)obj)->center;
    return ((GameObject *)obj)->sprite;
}
