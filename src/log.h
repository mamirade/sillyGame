#ifndef LOG_H
#define LOG_H

#include <stdbool.h>

#define LOG_ERROR(message) _log(message, __func__, __FILE__, __LINE__, "ERROR")
#define LOG_BUG(message) _log(message, __func__, __FILE__, __LINE__, "BUG")
#define LOG_INFO(message) _log(message, __func__, __FILE__, __LINE__, "INFO")

bool beginLog(void);

int _log(const char *message, const char *function_name, const char *file_name, int line, const char *messageType);

#endif /* LOG_H */
