#include <stddef.h>
#include "event.h"

/**
 *  Crée un nouvel évenement sans fonctions associées.
 *  Un évenement est créé sur la stack
 */
event initEvent() {
    return (event) {};
}

/**
 *  Ajoute la fonction aux fonctions appelées par l'évenement.
 */
event *subscribe(event *e, event_func f) {
    if (e->nextIndex >= MAX_CALLBACKS) {        
        return NULL;
    }

    e->callbacks[e->nextIndex] = f;
    e->nextIndex++;
    
    return e;
}

/**
 *  Enlève la fonction des fonctions appelées par l'évenement.
 */
event *unsubscribe(event *e, event_func f) {
    int i = 0;
    
    while (i < e->nextIndex && e->callbacks[i] != f) {
        i++;
    }    
    if (i >= e->nextIndex) {       
        return NULL;
    }
    
    e->nextIndex--;
    while (i < e->nextIndex) {
        e->callbacks[e->nextIndex] = e->callbacks[e->nextIndex+1];
        i++;
    }
    return e;
}

/**
 *  Appelle toutes les fonctions inscrite à l'évenement avec comme arguments
 *  les arguments variadiques passés comme va_list.
 */
void invoke(const event *e, ...) {
    va_list l;
    va_start(l, e);
    vinvoke(e, l);
    va_end(l);
}

void vinvoke(const event *e, va_list l) {
    va_list copy;
    int i;
    for (i = 0; i < e->nextIndex; ++i) {
        va_copy(copy, l);
        e->callbacks[i](copy);
        va_end(copy);
    }
}
