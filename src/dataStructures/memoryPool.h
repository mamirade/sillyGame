#ifndef MEMORYPOOL_H
#define MEMORYPOOL_H

#include <stddef.h>

#include "memoryManager.h"

typedef struct {
    // Bloc de mémoire dans lequel on peut récupérer de la mémoire
    void *memory;
    // Taille du bloc de mémoire
    size_t size;
    // Taille d'un bloc (tous les blocs donnés par cette structure sont de même taille)
    size_t blockSize;
    
    // Index de début de l'espace mémoire occupé contigu
    unsigned stackHead;    
    
    // Tête de la liste chainée de la mémoire disponible
    void *freeMemHead;
    // Nombre d'élément dans cette liste
    unsigned freeElemCount;

} memoryPool;

bool asMemoryPool(memoryPool *m, void *memory, size_t blockCount, size_t blockSize);
memoryPool *allocMemoryPool(size_t blockCount, size_t blockSize);
void freeMemoryPool(memoryPool *memPool);

void *getFromMemoryPool(memoryPool *memPool);
bool returnToMemoryPool(memoryPool *memPool, void *memory);

void clearMemoryPool(memoryPool *memPool);

bool memoryPoolManager(memoryManager *memManager, size_t blockCount, size_t blockSize);

#endif /* MEMORYPOOL_H */
