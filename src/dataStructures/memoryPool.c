#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "memoryPool.h"

bool asMemoryPool(memoryPool *m, void *memory, size_t blockCount, size_t blockSize) { 

    if (blockSize < sizeof(void *)) {
        return false;
    }
       
    m->memory = memory;
    m->size = blockCount* blockSize;
    m->blockSize = blockSize;
    m->stackHead = 0;
    m->freeMemHead = NULL;
    m->freeElemCount = 0;

    return true;
}

memoryPool *allocMemoryPool(size_t blockCount, size_t blockSize) {

    // Si on peut pas stocker de valeur de la taille d'un pointeur, notre structure ne marche pas
    // Pourrait fonctionner si on met un index de tableau au lieu d'un pointeur de suivant, mais on va supposer que si quelqu'un a besoin d'allouer des blocs plus petit qu'un pointeur, c'est refusé
    if (blockSize < sizeof(void *)) {
        return NULL;
    }
    
    memoryPool *memPool = malloc(sizeof(memoryPool));
    if (memPool != NULL) {
        
        memPool->memory = malloc(blockCount * blockSize);
        if (memPool->memory == NULL) {
            free(memPool);
            return NULL;
        }

        memPool->freeMemHead = NULL;
        memPool->size = blockCount*blockSize;
        memPool->blockSize = blockSize;
        memPool->freeElemCount = 0;
        memPool->stackHead = 0;
    }
    return memPool;
}

void freeMemoryPool(memoryPool *memPool) {
    free(memPool->memory);
    free(memPool);
}

void *getFromMemoryPool(memoryPool *memPool) {

    void *res = NULL;

    // Si on a un élément libre dans la liste chaînée de l'espace libre, on donne ça en priorité
    if (memPool->freeElemCount > 0) {
        res = memPool->freeMemHead;
        memPool->freeMemHead = *((void **) memPool->freeMemHead);
        memPool->freeElemCount--;
        memset(res, 0, memPool->blockSize);
    }
    // Si on a pas d'espace libre dans la liste chaînée, on passe de l'espace de la liste contiguë (si on peut)
    else if (memPool->stackHead < memPool->size) {
        // (char *) pour avancer d'un certain nombre d'octets
        res = ((char *) memPool->memory) + memPool->stackHead;
        memPool->stackHead += memPool->blockSize;
        memset(res, 0, memPool->blockSize);
    }

    return res;
}

bool returnToMemoryPool(memoryPool *memPool, void *memory) {    

    if (memory >= memPool->memory && memory < memPool->memory + memPool->size) {    
        *((void **) memory) = memPool->freeMemHead;
        memPool->freeMemHead = (void *) memory;
        memPool->freeElemCount++;
        return true;
    }
    return false;
}

static void *getMemory(memoryManager *memManager, size_t size) {
    (void) size;
    void *res = getFromMemoryPool(memManager->memorySource);
    memManager->allocFailed = (res == NULL);
    return res;
}

static void freeMemory(memoryManager *memManager, void *mem) {
    returnToMemoryPool(memManager->memorySource, mem);
}

static void freeManager(memoryManager *memManager) {
    freeMemoryPool(memManager->memorySource);
    memManager->memorySource = NULL;
}

bool memoryPoolManager(memoryManager *memManager, size_t blockCount, size_t blockSize) {

    memoryPool *memPool = allocMemoryPool(blockCount, blockSize);
    if (memPool == NULL) {
        return false;
    }
    
    memManager->memorySource = memPool;
    memManager->getMemory = getMemory;
    memManager->freeMemory = freeMemory;
    memManager->freeManager = freeManager;
    
    return true;
}

void clearMemoryPool(memoryPool *pool) {
    pool->stackHead = 0;
    pool->freeMemHead = NULL;
    pool->freeElemCount = 0;
}
