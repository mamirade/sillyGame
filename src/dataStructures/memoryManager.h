#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include <stddef.h>
#include <stdbool.h>

typedef struct memoryManager {
    // Pointeur vers une struct qui offre les informations nécéssaires aux méthodes
    bool allocFailed;
    void *memorySource;
    void *(*getMemory)(struct memoryManager *memManager, size_t size);
    void (*freeMemory)(struct memoryManager *memManager, void *mem);
    void (*freeManager)(struct memoryManager *memManager);
} memoryManager;

memoryManager stdlibMemoryManager();

#endif /* MEMORYMANAGER_H */
