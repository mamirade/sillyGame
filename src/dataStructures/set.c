#include "set.h"

static size_t hash(set *s, void *elem) {
    return ((size_t) elem) % s->size;
}

/**
 *  Crée un nouveau set à partir de l'array passé en paramètre
 *  Cette array est supposée de taille size et remplie de 0 à l'appel.
 */
set asSet(void *array[], size_t size) {
    return (set) {
        array,
        0,
        size
    };
}

/**
 *  Renvoie vrai si s contient aucun élément
 */
bool isEmptySet(const set *s) {
    return s->nbElem == 0;
}

/**
 *  Ajoute l'élément elem à s
 *  Si le set est plein ou que elem est NULL, renvoie NULL
 *  Sinon, renvoie s
 */
set *addSet(set *s, void *elem) {
    if (s->nbElem >= s->size || elem == NULL) {
        return NULL;
    }    
    size_t index = hash(s, elem);
    
    while (s->elems[index] != NULL && s->elems[index] != elem) {
        index = (index + 1) % s->size;
    }
    s->elems[index] = elem;
    s->nbElem++;
    
    return s;
}

/**
 *  Supprime l'élément elem de s
 *  Si s ne contient pas elem, renvoie NULL
 *  Sinon, renvoie s
 */
set *removeSet(set *s, void *elem) {
    size_t index = hash(s, elem);
    size_t visited = 0;

    while (s->elems[index] != elem) {
        index = (index+1) % s->size;
        visited++;
        if (visited >= s->size) {
            return NULL;
        }
    }
    s->elems[index] = NULL;
    s->nbElem--;
        
    return s;
}
