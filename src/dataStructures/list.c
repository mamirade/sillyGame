#include <assert.h>
#include "list.h"

/**
 *  Crée une liste à partir du tableau array supposé de taille size.
 */
list asList(void *array[], size_t size) {
    return (list) {
        array,
        0,
        size
    };
}

/**
 *  Renvoie vrai si l est vide.
 */
bool isEmptyList(const list *l) {
    return l->nbElem == 0;
}

/**
 *  Ajoute elem à la fin de la liste l.
 *  Renvoie la liste l si la liste n'est pas pleine et que l'ajout a pu se faire.
 *  Sinon, renvoie NULL.
 */
list *addEndList(list *l, void *elem) {
    if (l->nbElem >= l->size) {
        return NULL;
    }

    l->elems[l->nbElem] = elem;
    l->nbElem++;
    return l;
}

/**
 *  Supprime et renvoie le dernier élément de la liste
 *  Suppose l non vide.
 */
void *removeEndList(list *l) {
    assert(l->nbElem > 0);
    l->nbElem--;
    return l->elems[l->nbElem];
}

/**
 *  Supprime tous les éléments de la liste.
 *  NOTE: On bouge seulement un index qui indique le nombre
 *  d'éléments dans la liste
 */
void clearList(list *l) {
    l->nbElem = 0;
}
