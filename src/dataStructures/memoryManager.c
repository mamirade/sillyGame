#include "memoryManager.h"

#include <stdlib.h>

static void * dummyMalloc(memoryManager *m, size_t size) {
    (void)m;

    void *res = malloc(size);
    m->allocFailed = (res == NULL);
    
    return res;
}

static void dummyFree(memoryManager *m, void *mem) { 
    (void) m;
    free(mem);
    
}

static void dummyFreeManager(memoryManager *m) {
    (void) m;
}


/**
 *  Crée un memoryManager qui est en fait une simple indirection vers malloc et free. Il peut donc échouer à donner de la mémoire
 */
memoryManager stdlibMemoryManager() {
    memoryManager m;
    
    m.allocFailed = false;
    m.memorySource = NULL;
    m.getMemory = dummyMalloc;
    m.freeMemory = dummyFree;
    m.freeManager = dummyFreeManager;
    return m;
}
