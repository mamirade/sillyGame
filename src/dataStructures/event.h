#ifndef EVENT_H
#define EVENT_H

#include <stdbool.h>
#include <stdarg.h>

#define MAX_CALLBACKS 8

typedef void (*event_func)(va_list args);

typedef struct {
    event_func callbacks[MAX_CALLBACKS];
    int nextIndex;
} event;

event initEvent();

event *subscribe(event *e, event_func f);
event *unsubscribe(event *e, event_func f);

void invoke(const event *e, ...);
void vinvoke(const event *e, va_list l);
#endif /* EVENT_H */
