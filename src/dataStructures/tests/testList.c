#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../list.h"
#include "testhelp.h"

typedef union {
    void *ptr;
    int i;
} ptrToInt;
    
bool testListBasic(char errorMsg[]) {

    const int addedElem = 42;
    ptrToInt value = {};

    value.i = addedElem;
    
    int *array[8] = {};
    list l = asList((void **) array, 8);
    myassert(isEmptyList(&l));
    
    addEndList(&l, value.ptr);
    myassert(!isEmptyList(&l));    
    
    value.ptr = removeEndList(&l);
    myassert(isEmptyList(&l));

    if (value.i != addedElem) {
        snprintf(errorMsg, TESTLIB_BUFFER_SIZE, "Element added is different from element returned (%d != %d)", addedElem, value.i);        
        return false;
    }
    
    return true;
}

bool multipleAddRemove(char errorMsg[]) {

    const int nb_elem = 256;
    
    int *array[256] = {};
    list l = asList((void **) array, nb_elem);
    myassert(isEmptyList(&l));

    ptrToInt val = {};
    
    for (int i = 0; i < nb_elem; ++i) {
        val.i = i;
        addEndList(&l, val.ptr);
    }
    myassert(!isEmptyList(&l));
    myassert(addEndList(&l, (void *) 42) == NULL);

    for (int i = nb_elem-1; i >= 0; --i) {
        val.ptr = removeEndList(&l);
        if (val.i != i) {
            snprintf(errorMsg, TESTLIB_BUFFER_SIZE, "Unexpected value in list: (%d!=%d)", i, val.i);
            return false;
        }
    }
    myassert(isEmptyList(&l));
    return true;
}

void writeErrorMessage(char errorMsg[], int *array[], int *expected[]) {
    int i;
    ptrToInt vArray = {},
        vExp = {};
    for (i = 0; i < 10; ++i) {
        if (array[i] != expected[i]) {
            vArray.ptr = array[i];
            vExp.ptr = expected[i];
            snprintf(errorMsg, TESTLIB_BUFFER_SIZE, "Unexpected element in list: %d (instead of %d) at index %d", vArray.i, vExp.i, i);
            break;
        }
    }
}

bool chaos(char errorMsg[]) {
    int *expected[10] = {
        (void *)0,
        (void *)1,
        (void *)2,
        (void *)3,
        (void *)4,
        (void *)5,
        (void *)6,
        (void *)7,
        (void *)8,
        (void *)9,};
    
    unsigned long seed = time(NULL);
    printf("SEED: %ld\n", seed);
    srand(seed);
    
    int *array[10] = {};
    list l = asList((void **)array, 10);
    int i = 0;
    ptrToInt val = {};
    
    while (i < 10) {
        if (rand() < 0.51*RAND_MAX) {
            val.i = i;
            addEndList(&l, val.ptr);
            i++;
        }
        else if (!isEmptyList(&l)) {
            val.ptr = removeEndList(&l);
            i--;
            myassert(val.i == i);
        }
    }

    if (memcmp(array, expected, 10*sizeof(void *)) != 0) {
        writeErrorMessage(errorMsg, array, expected);
        return false;
    }
    
    return true;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    test t[] = {
        {testListBasic, "Basic tests"},
        {multipleAddRemove, "Many added then many removed"},
        {chaos, "Chaotic add and remove"}
    };
    startTests(t, sizeof(t)/sizeof(test), 0);
    
    return 0;
}
