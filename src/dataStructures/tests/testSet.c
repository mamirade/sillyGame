#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../set.h"
#include "testhelp.h"

typedef union {
    void *ptr;
    int i;
} ptrToInt;
    
bool testSetBasic(char errorMsg[]) {

    (void) errorMsg;
    const int addedElem = 42;
    ptrToInt value = {};

    value.i = addedElem;
    
    int *array[8] = {};
    set s = asSet((void **) array, 8);
    myassert(isEmptySet(&s));
    
    addSet(&s, value.ptr);
    myassert(!isEmptySet(&s));    
    
    removeSet(&s, value.ptr);
    myassert(isEmptySet(&s));
    
    return true;
}

bool multipleAddRemove(char errorMsg[]) {

    const int nb_elem = 256;    
    int *array[256] = {};
    
    set l = asSet((void **) array, nb_elem);
    myassert(isEmptySet(&l));

    ptrToInt val = {};
    
    for (int i = 1; i <= nb_elem; ++i) {
        val.i = i;
        addSet(&l, val.ptr);
    }
    myassert(!isEmptySet(&l));
    myassert(addSet(&l, (void *) 42) == NULL);

    for (int i = nb_elem; i >= 1; --i) {
        val.i = i;
        if (removeSet(&l, val.ptr) == NULL) {
            strncpy(errorMsg, "Value not in set", TESTLIB_BUFFER_SIZE);
            return false;
        }
    }
    myassert(isEmptySet(&l));
    return true;
}

int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    test t[] = {
        {testSetBasic, "Basic tests"},
        {multipleAddRemove, "Many added then many removed"},
    };
    startTests(t, sizeof(t)/sizeof(test), 0);
    
    return 0;
}
