#include "../event.h"
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <SDL2/SDL.h>

bool isInRect(SDL_Rect r, int x, int y) {
    int xDiff = x - r.x;
    int yDiff = y - r.y;

    return xDiff > 0 && xDiff < r.w && yDiff > 0 && yDiff < r.h;
}

void testEvent(SDL_Renderer *rend, event inRect) {
    
    bool isRunning = true;
    SDL_Event e;

    SDL_SetRenderDrawColor(rend, 255, 255, 255, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(rend);
    SDL_RenderPresent(rend);
    
    SDL_Rect randomRect = {};
    randomRect.w = 100;
    randomRect.h = 50;
    randomRect.x = (rand() % (640 - randomRect.w)) + randomRect.w/2;
    randomRect.y = (rand() % (480 - randomRect.h)) + randomRect.h/2;

    printf("Rect: (%d, %d)\n", randomRect.x + randomRect.w/2, randomRect.y + randomRect.h/2);
    
    while (isRunning) {
        SDL_RenderClear(rend);
        while (SDL_PollEvent(&e)) {
            switch (e.type) {
            case SDL_QUIT:
                isRunning = false;
                break;
            case SDL_MOUSEBUTTONDOWN:
                printf("(%d, %d)\n", e.button.x, e.button.y);     
                if (isInRect(randomRect, e.button.x, e.button.y)) {
                    invoke(&inRect, rend, randomRect, &isRunning);
                }           
                break;
            }
        }
        SDL_RenderPresent(rend);
        SDL_Delay(10);
    }
    SDL_Delay(1000);
}

void showRectangle(va_list args) {
    SDL_Renderer *rend = va_arg(args, SDL_Renderer *);
    const SDL_Rect rect = va_arg(args, SDL_Rect);

    Uint8 r,
        g,
        b,
        a;
    SDL_GetRenderDrawColor(rend, &r, &g, &b, &a);
    SDL_SetRenderDrawColor(rend, 255, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderFillRect(rend, &rect);
    SDL_SetRenderDrawColor(rend, r, g, b, a);
}

void printMessage(va_list args) {

    static bool hasBeenHereOnce = false;
    
    (void) va_arg(args, SDL_Renderer *);
    SDL_Rect r = va_arg(args, SDL_Rect);

    if (hasBeenHereOnce) {
        puts("Fonction unsubscribe n'a pas marché");
    }
    else {
        printf("Trouvé ! centre: (%d, %d)\n", r.x + r.w/2, r.y + r.h/2);
        hasBeenHereOnce = true;
    }
}

void endGame(va_list args) {
    (void) va_arg(args, SDL_Renderer *);
    (void) va_arg(args, SDL_Rect);
    bool *isRunning = va_arg(args, bool *);
    *isRunning = false;
}

int main()
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        puts(SDL_GetError());
        return EXIT_FAILURE;
    }
    SDL_Window *window = SDL_CreateWindow(
        "Testing events", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, 0);
    if (window == NULL) {
        puts(SDL_GetError());
        SDL_Quit();
        return EXIT_FAILURE;
    }
    SDL_Renderer *rend = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_ACCELERATED);
    if (rend == NULL) {
        puts(SDL_GetError());
        SDL_DestroyWindow(window);
        SDL_Quit();
        return EXIT_FAILURE;
    }

    srand(time(NULL));

    event e = initEvent();

    subscribe(
        subscribe(
            subscribe(
                subscribe(&e,
                          // printMessage inclus 2 fois
                          showRectangle),
                          printMessage),
                          endGame),
                          printMessage);

    unsubscribe(&e, printMessage);
    
    testEvent(rend, e);
    
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
