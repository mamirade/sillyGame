#include <stdio.h>
#include <string.h>

#include "../memoryPool.h"
#include "testhelp.h"

typedef struct {
    double x;
    double y;
} VectorDouble;

void printArray(VectorDouble *array, int n) {
    int i;
    printf("{(%.3lf, %.3lf)", array[0].x, array[0].y);
    for (i = 1; i < n; ++i) {
        printf(", (%.3lf, %.3lf)", array[i].x, array[i].y);
    }
    puts("}");
}

bool testAddRemove(char errorMsg[]) {

    (void) errorMsg;
    
    VectorDouble array[16] = {0};
    memoryPool pool;
    myassert(asMemoryPool(&pool, array, 16, sizeof(*array)));
    myassert(pool.freeElemCount == 0);
    myassert(!returnToMemoryPool(&pool, NULL));
    myassert(!returnToMemoryPool(&pool, array-1));
    myassert(!returnToMemoryPool(&pool, array+16));
    
    VectorDouble *tmp1 = getFromMemoryPool(&pool);
    myassert(tmp1 == array);
    tmp1->x = 42;
    tmp1->y = -42;
    
    VectorDouble *tmp2 = getFromMemoryPool(&pool);
    myassert(tmp2 == array + 1);
    tmp2->x = -69;
    tmp2->y = 69;

    VectorDouble *tmp3 = getFromMemoryPool(&pool);
    myassert(tmp3 == array + 2);
    tmp3->x = 4;
    printArray(array, 4);
    
    returnToMemoryPool(&pool, tmp2);
    returnToMemoryPool(&pool, tmp3);
    
    tmp2 = getFromMemoryPool(&pool);
    myassert(tmp2 == array+2);
    myassert(tmp2->x == 0 && tmp2->y == 0);
    
    tmp2->x = 61;
    tmp2->y = 43;    
    myassert(tmp1->x == 42 && tmp1->y == -42);
    printArray(array, 4);

    tmp3 = getFromMemoryPool(&pool);
    myassert(tmp3 == array+1);
    tmp3->x = 4;
    printArray(array, 4);

    myassert(pool.freeElemCount == 0);

    VectorDouble *tmp4 = getFromMemoryPool(&pool);
    myassert(tmp4 == array + 3);
    tmp4->x = 123456789;
    tmp4->y = 3.141592;
    printArray(array, 4);
    
    return true;
}



int main(int argc, char *argv[])
{
    (void) argc;
    (void) argv;

    test t[] = {
        {testAddRemove, "Basic tests"},
    };
    startTests(t, sizeof(t)/sizeof(test), 0);
}
