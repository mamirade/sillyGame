#ifndef LIST_H
#define LIST_H

#include <stdbool.h>
#include <stddef.h>

/**
 *  Structure de liste contiguë générique.
 */
typedef struct {
    void ** const elems;
    size_t nbElem;
    const size_t size;
} list;

list asList(void *array[], size_t size);

bool isEmptyList(const list *l);

list *addEndList(list *l, void *elem);
void *removeEndList(list *l);

void clearList(list *l);
#endif /* LIST_H */

