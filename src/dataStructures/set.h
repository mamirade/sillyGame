#ifndef SET_H
#define SET_H

#include <stdbool.h>
#include <stddef.h>

typedef struct {
    void ** const elems;
    size_t nbElem;
    const size_t size;
} set;

set asSet(void *array[], size_t size);

bool isEmptySet(const set *s);

set *addSet(set *s, void *elem);
set *removeSet(set *s, void *elem);

#endif /* SET_H */
