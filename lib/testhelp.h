#ifndef MYTESTLIB_H
#define MYTESTLIB_H

#include <stdbool.h>

typedef enum {
    HIDE_SUCCESS = 0x1,
    CLEAR_TERM = 0x10,
} testFlags;

typedef bool (*testFunc)(char errorMessage[]);

typedef struct {
    testFunc f;
    char *name;
} test;

#define TESTLIB_BUFFER_SIZE 256

#define myassert(COND) _myassert(COND, #COND, __func__, __FILE__, __LINE__)
void startTests(test tests[], int nb_tests, int flags);
void _myassert(bool cond, const char *condText, const char *func, const char *file, int line);

#endif /* MYTESTLIB_H */
