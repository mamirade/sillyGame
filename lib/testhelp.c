#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "testhelp.h"

#define BLACK(TEXT) "\033[30m" TEXT "\033[0m"
#define RED(TEXT) "\033[31m" TEXT "\033[0m"
#define GREEN(TEXT) "\033[32m" TEXT "\033[0m"
#define YELLOW(TEXT) "\033[33m" TEXT "\033[0m"
#define BLUE(TEXT) "\033[34m" TEXT "\033[0m"
#define PINK(TEXT) "\033[35m" TEXT "\033[0m"
#define CYAN(TEXT) "\033[36m" TEXT "\033[0m"
#define WHITE(TEXT) "\033[37m" TEXT "\033[0m"

typedef void (*resultPrinter)(char *, int, int, char*, clock_t);

void _myassert(bool cond, const char *condText, const char *func, const char *file, int line) {
    if (!cond) {
        fprintf(stderr, "\033[4;5;31mAssertion failed\033[0m: %s\n", condText);
        fprintf(stderr, "  in file %s: %s (line %d)\n", file, func, line);
        
        exit(EXIT_FAILURE);
    }
}

char *formatTime(clock_t diff, char buffer[]) {

    char *units[] = {"s", "ms", "µs", "ns", NULL};
    int unitIndex = 0;
    
    double time = diff;

    while (time < CLOCKS_PER_SEC && units[unitIndex+1] != NULL) {
        time *= 1000;
        unitIndex++;
    }

    snprintf(buffer, TESTLIB_BUFFER_SIZE, "%.2lf%s", time/CLOCKS_PER_SEC, units[unitIndex]);
    return buffer;
}

void printSuccess(char *testName, int testNumber, clock_t diff) {
    char buffer[TESTLIB_BUFFER_SIZE];
    
    fprintf(stderr, GREEN("%d: PASSED (%s)\n"), testNumber, testName);
    fprintf(stderr, "  Time taken: " PINK("%s\n"), formatTime(diff, buffer));
    fputs("-----\n", stderr);
}

void printFailure(char *testName, int testNumber, char *errorMessage) {
    fprintf(stderr, RED("%d: FAILED (%s)\n"), testNumber, testName);
    fprintf(stderr, "  Reason: " PINK("%s\n"), errorMessage);
    fputs("-----\n", stderr);
}

void printAll(char *testName, int testNumber, int success, char *errorMessage, clock_t diff) {    
    if (success) {
        printSuccess(testName, testNumber, diff);
    }
    else {
        printFailure(testName, testNumber, errorMessage);
    }    
}

void printOnlyFailures(char *testName, int testNumber, int success, char *errorMessage, clock_t diff) {
    (void) diff;
    if (!success) {
        printFailure(testName, testNumber, errorMessage);
    }
}

void handleFlags(int flags, resultPrinter *p) {
    if (flags & HIDE_SUCCESS) {
        *p = printOnlyFailures;
    }
    else {
        *p = printAll;
    }

    if (flags & CLEAR_TERM) {
#if defined(WIN32_)
        system("cls");
#else
        system("clear");
#endif
    }
}

void startTests(test tests[], int nb_tests, int flags) {
    
    int successes = 0;
    
    clock_t start;
    clock_t end;
    int res;

    resultPrinter printer;    
    handleFlags(flags, &printer);

    for (int i = 0; i < nb_tests; ++i) {
        
        char errorMessage[TESTLIB_BUFFER_SIZE] = "";
        
        start = clock();
        res = tests[i].f(errorMessage);
        end = clock();

        printer(tests[i].name, i, res, errorMessage, end - start);        
        successes += res;
    }

    if (successes == nb_tests) {
        fputs("\033[4;5;32mAll tests passed!\n\033[0m", stderr);
    }
    else {
        fprintf(stderr, "\033[4;5;31mSOME TESTS FAILED\033[0m (" PINK("%d/%d") " tests passed)\n", successes, nb_tests);
    }
}
