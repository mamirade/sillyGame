#include "testhelp.h"
#include <string.h>
#include <unistd.h>

bool func1(char errorMsg[]) {
    (void) errorMsg;
    return true;
}

bool func2(char errorMsg[]) {
    strncpy(errorMsg, "je suis un petit rigolo :)", TESTLIB_BUFFER_SIZE);
    return false;
}

bool func3(char errorMsg[]) {
    (void) errorMsg;
    return false;
}

bool func4(char errorMsg[]) {
    (void) errorMsg;
    int i;
    for (i = 0; i < 1000000000; ++i);
    
    return true;
}

int main(int argc, char *argv[])
{

    (void) argc;
    (void) argv;
    
    // Liste de tests avec leurs noms
    test tests[] = {
        {func1, "Réussite"},
        {func2, "Echec"},
        {func3, "Echec sans message d'erreur"},
        {func4, "Long..."}
    };

    startTests(
        // Liste de tests
        tests,
        // Nombre de tests
        4,
        // flags
        CLEAR_TERM);
    
    return 0;
}
